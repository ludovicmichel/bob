export default {
  type: "FeatureCollection",
  name: "horta",
  crs: { type: "name", properties: { name: "urn:ogc:def:crs:OGC:1.3:CRS84" } },
  features: [
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 516.86576091899997,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15409,
        UNIDADEAMO: "AHR_UA_APRON_A_2_33",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2904
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715031325531861, 38.520694278408101],
              [-28.715031171213791, 38.520888995310706],
              [-28.714754958418364, 38.520888859888586],
              [-28.714756884054552, 38.520771028642791],
              [-28.714751795762698, 38.520737664334057],
              [-28.71475812868071, 38.520701966286509],
              [-28.714757256408774, 38.520696185104029],
              [-28.715031325531861, 38.520694278408101]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 474.23895950100001,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15410,
        UNIDADEAMO: "AHR_UA_APRON_A_2_1",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716026671679497, 38.520688347684356],
              [-28.716087403027657, 38.520665412294335],
              [-28.716134211262958, 38.520636062932851],
              [-28.716161638769002, 38.520620208641375],
              [-28.71618878386116, 38.520596237749515],
              [-28.716196725707917, 38.520579143956674],
              [-28.716197326356038, 38.520578052203383],
              [-28.716198656932896, 38.520578039847294],
              [-28.71650663433762, 38.520576932330236],
              [-28.716514571081095, 38.52060365315419],
              [-28.716529506407927, 38.520626268467581],
              [-28.716559655102508, 38.520658742736813],
              [-28.716590817317936, 38.520683044552818],
              [-28.716624030304914, 38.520693652116307],
              [-28.716668326986763, 38.520701336146338],
              [-28.716026671679497, 38.520688347684356]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 407.83877505800001,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15411,
        UNIDADEAMO: "AHR_UA_APRON_A_2_2",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2904
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.714757256408774, 38.520696185104029],
              [-28.714740199944586, 38.520583139260111],
              [-28.714740065396605, 38.520580095336271],
              [-28.714740384435238, 38.520580086813197],
              [-28.714831532327636, 38.520580487495515],
              [-28.715046540427402, 38.520581580514403],
              [-28.715053253921429, 38.52058162406118],
              [-28.715073859891923, 38.520607351983152],
              [-28.715119229873476, 38.520648167667041],
              [-28.715163959764794, 38.52066856409828],
              [-28.715218376256264, 38.52069297672984],
              [-28.714757256408774, 38.520696185104029]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 591.61091574600005,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15412,
        UNIDADEAMO: "AHR_UA_APRON_A_2_3",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 6.28 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716733532144467, 38.521332771258514],
              [-28.716733725761266, 38.52108091064899],
              [-28.71697620384705, 38.521081025517276],
              [-28.716977992265264, 38.521331306501978],
              [-28.716733532144467, 38.521332771258514]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 447.05423365199999,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15413,
        UNIDADEAMO: "AHR_UA_APRON_A_2_4",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716733725761266, 38.52108091064899],
              [-28.716733872685843, 38.520889815719009],
              [-28.716974839760862, 38.520889929822637],
              [-28.71697620384705, 38.521081025517276],
              [-28.716733725761266, 38.52108091064899]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 437.35541203499997,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15414,
        UNIDADEAMO: "AHR_UA_APRON_A_2_5",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716733872685843, 38.520889815719009],
              [-28.716734017506198, 38.520701480810303],
              [-28.716920950447214, 38.520701892273124],
              [-28.716973634397004, 38.520702128843958],
              [-28.716973513019195, 38.520704062220148],
              [-28.716974839760862, 38.520889929822637],
              [-28.716733872685843, 38.520889815719009]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 580.72815154900002,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15415,
        UNIDADEAMO: "AHR_UA_APRON_A_2_6",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716490320811047, 38.521289691342659],
              [-28.716490482105016, 38.521080794913267],
              [-28.716733725761266, 38.52108091064899],
              [-28.716733532144467, 38.521332771258514],
              [-28.716567225493588, 38.521333767443203],
              [-28.71654304334637, 38.52132536494431],
              [-28.716518380836558, 38.521312862092387],
              [-28.716500535394644, 38.521294032742993],
              [-28.71649207779393, 38.521290389810332],
              [-28.716490320811047, 38.521289691342659]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 449.86660858499999,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15416,
        UNIDADEAMO: "AHR_UA_APRON_A_2_7",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716490482105016, 38.521080794913267],
              [-28.716490629673082, 38.520889700034509],
              [-28.716733872685843, 38.520889815719009],
              [-28.716733725761266, 38.52108091064899],
              [-28.716490482105016, 38.521080794913267]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 446.615214439,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15417,
        UNIDADEAMO: "AHR_UA_APRON_A_2_8",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716490629673082, 38.520889700034509],
              [-28.716490777925618, 38.52069774253021],
              [-28.716668326986763, 38.520701336146338],
              [-28.716734017506198, 38.520701480810303],
              [-28.716733872685843, 38.520889815719009],
              [-28.716490629673082, 38.520889700034509]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 465.76036336599998,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15418,
        UNIDADEAMO: "AHR_UA_APRON_A_2_9",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716247119351245, 38.521277347149066],
              [-28.716247307312077, 38.521080678704955],
              [-28.716490482105016, 38.521080794913267],
              [-28.716490320811047, 38.521289691342659],
              [-28.716468883674217, 38.521281169260611],
              [-28.716420826421214, 38.521277757686718],
              [-28.716247119351245, 38.521277347149066]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 449.70721238099998,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15419,
        UNIDADEAMO: "AHR_UA_APRON_A_2_10",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716247307312077, 38.521080678704955],
              [-28.716247489964374, 38.520889583893883],
              [-28.716490629673082, 38.520889700034509],
              [-28.716490482105016, 38.521080794913267],
              [-28.716247307312077, 38.521080678704955]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 457.32543944600002,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15420,
        UNIDADEAMO: "AHR_UA_APRON_A_2_11",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 11.39 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716247489964374, 38.520889583893883],
              [-28.716247678052479, 38.520692821719294],
              [-28.716490777925618, 38.52069774253021],
              [-28.716490629673082, 38.520889700034509],
              [-28.716247489964374, 38.520889583893883]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 462.54501469299998,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15421,
        UNIDADEAMO: "AHR_UA_APRON_A_2_12",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716003841917377, 38.521276771756014],
              [-28.716003994737839, 38.52108056192511],
              [-28.716247307312077, 38.521080678704955],
              [-28.716247119351245, 38.521277347149066],
              [-28.716003841917377, 38.521276771756014]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 450.02553976500002,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15422,
        UNIDADEAMO: "AHR_UA_APRON_A_2_13",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716003994737839, 38.52108056192511],
              [-28.716004143592855, 38.520889467148784],
              [-28.716247489964374, 38.520889583893883],
              [-28.716247307312077, 38.521080678704955],
              [-28.716003994737839, 38.52108056192511]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 468.50360008600001,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15423,
        UNIDADEAMO: "AHR_UA_APRON_A_2_14",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716004143592855, 38.520889467148784],
              [-28.716004296309158, 38.52069343967581],
              [-28.716026671679497, 38.520688347684356],
              [-28.716247678052479, 38.520692821719294],
              [-28.716247489964374, 38.520889583893883],
              [-28.716004143592855, 38.520889467148784]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 461.36806719800001,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15424,
        UNIDADEAMO: "AHR_UA_APRON_A_2_15",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715760597904357, 38.521276195936359],
              [-28.715760751026906, 38.52108044467272],
              [-28.716003994737839, 38.52108056192511],
              [-28.716003841917377, 38.521276771756014],
              [-28.715760597904357, 38.521276195936359]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 449.86614356199999,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15425,
        UNIDADEAMO: "AHR_UA_APRON_A_2_16",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715760751026906, 38.52108044467272],
              [-28.7157609005254, 38.520889349947588],
              [-28.716004143592855, 38.520889467148784],
              [-28.716003994737839, 38.52108056192511],
              [-28.715760751026906, 38.52108044467272]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 432.70981789400003,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15426,
        UNIDADEAMO: "AHR_UA_APRON_A_2_17",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.7157609005254, 38.520889349947588],
              [-28.715761042734329, 38.520707594628369],
              [-28.715929362103005, 38.520707756169045],
              [-28.715985433746066, 38.520697732249374],
              [-28.716004296309158, 38.52069343967581],
              [-28.716004143592855, 38.520889467148784],
              [-28.7157609005254, 38.520889349947588]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 460.28849471500001,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15427,
        UNIDADEAMO: "AHR_UA_APRON_A_2_18",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715517353876251, 38.521275619611075],
              [-28.715517507297761, 38.52108032691477],
              [-28.715760751026906, 38.52108044467272],
              [-28.715760597904357, 38.521276195936359],
              [-28.715517353876251, 38.521275619611075]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 449.86598853499999,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15428,
        UNIDADEAMO: "AHR_UA_APRON_A_2_19",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 14.102 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715517507297761, 38.52108032691477],
              [-28.715517657439733, 38.520889232240854],
              [-28.7157609005254, 38.520889349947588],
              [-28.715760751026906, 38.52108044467272],
              [-28.715517507297761, 38.52108032691477]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 428.01639153299999,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15429,
        UNIDADEAMO: "AHR_UA_APRON_A_2_20",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715517657439733, 38.520889232240854],
              [-28.715517800352004, 38.520707360754351],
              [-28.715761042734329, 38.520707594628369],
              [-28.7157609005254, 38.520889349947588],
              [-28.715517657439733, 38.520889232240854]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 459.20892225400002,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15430,
        UNIDADEAMO: "AHR_UA_APRON_A_2_21",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715274109833018, 38.521275042780175],
              [-28.71527426355042, 38.521080208651206],
              [-28.715517507297761, 38.52108032691477],
              [-28.715517353876251, 38.521275619611075],
              [-28.715274109833018, 38.521275042780175]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 449.86583351000002,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15431,
        UNIDADEAMO: "AHR_UA_APRON_A_2_22",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.71527426355042, 38.521080208651206],
              [-28.71527441433587, 38.520889114028556],
              [-28.715517657439733, 38.520889232240854],
              [-28.715517507297761, 38.52108032691477],
              [-28.71527426355042, 38.521080208651206]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 429.15217927800001,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15432,
        UNIDADEAMO: "AHR_UA_APRON_A_2_23",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 6.18 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.71527441433587, 38.520889114028556],
              [-28.715274559998644, 38.52070453341085],
              [-28.715316984702277, 38.52070645105136],
              [-28.715369768767594, 38.520707218176675],
              [-28.715517800352004, 38.520707360754351],
              [-28.715517657439733, 38.520889232240854],
              [-28.71527441433587, 38.520889114028556]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 269.125678236,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15433,
        UNIDADEAMO: "AHR_UA_APRON_A_2_24",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2904
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715031019784867, 38.521080089882147],
              [-28.715030868069825, 38.521271568569567],
              [-28.71502699550744, 38.521270951761366],
              [-28.715003852214952, 38.521263599071276],
              [-28.714966625229806, 38.52124277312128],
              [-28.714934691127919, 38.521218973878604],
              [-28.714811529107834, 38.521116805125253],
              [-28.714776971102374, 38.521079965297567],
              [-28.715031019784867, 38.521080089882147]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 457.90739249500001,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15434,
        UNIDADEAMO: "AHR_UA_APRON_A_2_25",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2904
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715030868069825, 38.521271568569567],
              [-28.715031019784867, 38.521080089882147],
              [-28.71527426355042, 38.521080208651206],
              [-28.715274109833018, 38.521275042780175],
              [-28.715081760713705, 38.521274586283894],
              [-28.715050829388925, 38.521274747936431],
              [-28.715030868069825, 38.521271568569567]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 449.86567849300002,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15435,
        UNIDADEAMO: "AHR_UA_APRON_A_2_26",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 10.72 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2904
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715031019784867, 38.521080089882147],
              [-28.715031171213791, 38.520888995310706],
              [-28.71527441433587, 38.520889114028556],
              [-28.71527426355042, 38.521080208651206],
              [-28.715031019784867, 38.521080089882147]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 509.341859978,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15436,
        UNIDADEAMO: "AHR_UA_APRON_A_2_27",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2904
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715031171213791, 38.520888995310706],
              [-28.715031019784867, 38.521080089882147],
              [-28.714776971102374, 38.521079965297567],
              [-28.714776106998244, 38.521079042956472],
              [-28.714761635769051, 38.521063456205944],
              [-28.714753433209438, 38.521037103252759],
              [-28.714753097302459, 38.521002743136428],
              [-28.714754958418364, 38.520888859888586],
              [-28.715031171213791, 38.520888995310706]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 456.76241269000002,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 620,
        IDUNIDADEA: 15437,
        UNIDADEAMO: "AHR_UA_APRON_A_2_28",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2904
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715031171213791, 38.520888995310706],
              [-28.715031325531861, 38.520694278408101],
              [-28.715218376256264, 38.52069297672984],
              [-28.715262778002192, 38.520704000849697],
              [-28.715274559998644, 38.52070453341085],
              [-28.71527441433587, 38.520889114028556],
              [-28.715031171213791, 38.520888995310706]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 84.558399244100002,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 621,
        IDUNIDADEA: 15438,
        UNIDADEAMO: "AHR_UA_APRON_A_1_29",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2904
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.714642215925796, 38.520582631282927],
              [-28.714740066742237, 38.520580095047755],
              [-28.714740200012137, 38.520583138831242],
              [-28.714758129541586, 38.520701965311886],
              [-28.71475179610329, 38.520737663167324],
              [-28.714749774243032, 38.520724404318223],
              [-28.714735541842561, 38.520690292847931],
              [-28.71470381371973, 38.520658727834281],
              [-28.714681724074062, 38.520646439353698],
              [-28.714642226362695, 38.520634402538455],
              [-28.714642215925796, 38.520582631282927]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 308.214955089,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 621,
        IDUNIDADEA: 15439,
        UNIDADEAMO: "AHR_UA_APRON_A_1_30",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 16.46 }],
        insp: "Sim",
        BERMA: 0,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716514571081095, 38.52060365315419],
              [-28.716506634336032, 38.520576932057757],
              [-28.716510747702507, 38.520576917117907],
              [-28.716597129795687, 38.520576422154853],
              [-28.71660642446626, 38.520598033370796],
              [-28.716623935700735, 38.520615708760303],
              [-28.716657827959281, 38.520632513091549],
              [-28.716694546732569, 38.520638012537283],
              [-28.716748217119907, 38.520637868445554],
              [-28.716977897383568, 38.520634758009109],
              [-28.716973634397004, 38.520702128843958],
              [-28.716920950447214, 38.520701892273124],
              [-28.716668326986763, 38.520701336146338],
              [-28.716624030304914, 38.520693652116307],
              [-28.716590817317936, 38.520683044552818],
              [-28.716559655102508, 38.520658742736813],
              [-28.716529506407927, 38.520626268467581],
              [-28.716514571081095, 38.52060365315419]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 357.92001981800001,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 621,
        IDUNIDADEA: 15440,
        UNIDADEAMO: "AHR_UA_APRON_A_1_31",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715614174140967, 38.52064157552396],
              [-28.715619030684842, 38.520707457255021],
              [-28.715369769644003, 38.520707217026327],
              [-28.715316985367515, 38.520706450800503],
              [-28.715262779045517, 38.520704000020046],
              [-28.715218376249702, 38.520692976878109],
              [-28.715163960787052, 38.520668562824525],
              [-28.715119230807186, 38.520648166589623],
              [-28.715073859942674, 38.520607351553522],
              [-28.71505325507389, 38.52058162476785],
              [-28.715160809446992, 38.520582333561762],
              [-28.71516530233945, 38.520582085686684],
              [-28.715208178616628, 38.520611192642825],
              [-28.715234120424228, 38.520621572186478],
              [-28.715278516955067, 38.520636330468491],
              [-28.715336313971598, 38.520640710252636],
              [-28.715448972914512, 38.520641582859142],
              [-28.715614174140967, 38.52064157552396]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 367.09286,
        IDZONA: 504,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 621,
        IDUNIDADEA: 15441,
        UNIDADEAMO: "AHR_UA_APRON_A_1_32",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2900
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715619030684842, 38.520707457255021],
              [-28.715614174140967, 38.52064157552396],
              [-28.715624516165406, 38.520641575057006],
              [-28.715931814678179, 38.520639372402265],
              [-28.715972971083684, 38.520633632701447],
              [-28.716015216011922, 38.520620577994457],
              [-28.716050178696698, 38.520605287574469],
              [-28.716089937964025, 38.520579044002602],
              [-28.716197326028574, 38.5205780521405],
              [-28.716196725576253, 38.520579143252291],
              [-28.716188784014125, 38.520596237823156],
              [-28.716161638689467, 38.520620209197844],
              [-28.716134212778687, 38.520636062450222],
              [-28.716087403888913, 38.520665412477584],
              [-28.716026671726375, 38.52068834777652],
              [-28.715985434791452, 38.520697731921992],
              [-28.715929361762615, 38.520707755598863],
              [-28.715619030684842, 38.520707457255021]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 472.55267722000002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15155,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_0",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.712978911997482, 38.519716481653127],
              [-28.712973606720926, 38.51964239593223],
              [-28.713631155213939, 38.519639240276263],
              [-28.713631741023441, 38.519714173627371],
              [-28.712978911997482, 38.519716481653127]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 563.81688139400001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15156,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_1",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.717075294951176, 38.520101771840551],
              [-28.717073693158145, 38.519898110029928],
              [-28.717360395306525, 38.51989723040964],
              [-28.717361211328921, 38.520000839882833],
              [-28.717361990250673, 38.520099942657602],
              [-28.717075294951176, 38.520101771840551]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 451.13380020099999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 617,
        IDUNIDADEA: 15157,
        UNIDADEAMO: "AHR_UA_RWY_1028_5_2",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2908
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.706413927230297, 38.520319230407551],
              [-28.706409593412289, 38.520142625197117],
              [-28.706673439175788, 38.520140889457771],
              [-28.706674148840232, 38.520320056707916],
              [-28.706418501093165, 38.520319244936246],
              [-28.706413927230297, 38.520319230407551]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 526.20694191999996,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 618,
        IDUNIDADEA: 15158,
        UNIDADEAMO: "AHR_UA_RWY_1028_6_3",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 90.73 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2907
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.725074548462491, 38.520067106625092],
              [-28.725074130391953, 38.520244700337976],
              [-28.724773946088895, 38.520248252510342],
              [-28.724766976042989, 38.520068057136299],
              [-28.725074548462491, 38.520067106625092]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 434.69517997899999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 616,
        IDUNIDADEA: 15159,
        UNIDADEAMO: "AHR_UA_RWY_1028_4_4",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716755785363969, 38.520108694809281],
              [-28.716784604285142, 38.520103623022891],
              [-28.717361990250673, 38.520099942657602],
              [-28.717362514020881, 38.520166455139858],
              [-28.716620676042247, 38.520169850487122],
              [-28.716627426925669, 38.520164595755297],
              [-28.716648788930481, 38.520147871093911],
              [-28.716687518647003, 38.520129526102743],
              [-28.716730701528615, 38.520115215488211],
              [-28.716755785363969, 38.520108694809281]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 582.62100668599999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 616,
        IDUNIDADEA: 15160,
        UNIDADEAMO: "AHR_UA_RWY_1028_4_5",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715224537364765, 38.52012234046007],
              [-28.71527758756649, 38.520107351980151],
              [-28.715981158445928, 38.520105643844687],
              [-28.71602932175697, 38.520121396773618],
              [-28.716063543029975, 38.520133204668063],
              [-28.716091714413199, 38.520146466884611],
              [-28.716115498266024, 38.520158427764173],
              [-28.716128695652518, 38.520164384940109],
              [-28.716137329694579, 38.520172059830138],
              [-28.715954570813366, 38.520172893832459],
              [-28.715106185121019, 38.520175885255476],
              [-28.715142372546389, 38.520154516363014],
              [-28.715177351375011, 38.520139052166954],
              [-28.715224537364765, 38.52012234046007]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 613.50435580199996,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 616,
        IDUNIDADEA: 15161,
        UNIDADEAMO: "AHR_UA_RWY_1028_4_6",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.713635391084175, 38.52018105627014],
              [-28.71363484588656, 38.520111320846041],
              [-28.714441621165179, 38.520109374568776],
              [-28.714478115496124, 38.520119388987283],
              [-28.714505445905743, 38.520125398416774],
              [-28.714546521982648, 38.520137611265149],
              [-28.714582210350567, 38.520152042503845],
              [-28.714611128547087, 38.520167638787385],
              [-28.714622485131713, 38.520177588284049],
              [-28.713635391084175, 38.52018105627014]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 450.81610730800003,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 616,
        IDUNIDADEA: 15162,
        UNIDADEAMO: "AHR_UA_RWY_1028_4_7",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.714874648526212, 38.519633262501287],
              [-28.714876464166156, 38.519709763250361],
              [-28.714264540260384, 38.519711933109647],
              [-28.714262743616786, 38.519636205727963],
              [-28.714874648526212, 38.519633262501287]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 493.32485613599999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 618,
        IDUNIDADEA: 15163,
        UNIDADEAMO: "AHR_UA_RWY_1028_6_8",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2907
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.725194561318837, 38.520243275025678],
              [-28.725196870699431, 38.520417336321884],
              [-28.725146346510204, 38.520415965247679],
              [-28.72504367930577, 38.52039578675361],
              [-28.724986793736772, 38.520375524286017],
              [-28.724805654640694, 38.520291941924],
              [-28.724756358553105, 38.520270053928144],
              [-28.72470898883504, 38.520249021067968],
              [-28.725194561318837, 38.520243275025678]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 497.81218431799999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 618,
        IDUNIDADEA: 15164,
        UNIDADEAMO: "AHR_UA_RWY_1028_6_9",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2907
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.725196870699431, 38.520417336321884],
              [-28.725194561318837, 38.520243275025678],
              [-28.72521237480569, 38.520243064190744],
              [-28.725608259208681, 38.520238377925075],
              [-28.725584786799313, 38.52026849320778],
              [-28.725576492856305, 38.520279133078908],
              [-28.725525405573279, 38.520321907066574],
              [-28.725437803970394, 38.520367971406934],
              [-28.725382411272257, 38.52038791840836],
              [-28.725316301395935, 38.520404631491708],
              [-28.725233942183422, 38.52041834231629],
              [-28.725216548715263, 38.520417870317999],
              [-28.725196870699431, 38.520417336321884]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 483.86424892399998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 618,
        IDUNIDADEA: 15165,
        UNIDADEAMO: "AHR_UA_RWY_1028_6_10",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2907
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.725368448047579, 38.520241216839942],
              [-28.725364269015238, 38.520066210473374],
              [-28.72567433920219, 38.520065250624491],
              [-28.725674312877295, 38.520113129469038],
              [-28.725638660491597, 38.52019937280064],
              [-28.725608259208681, 38.520238377925075],
              [-28.725368448047579, 38.520241216839942]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 499.38335219200002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 618,
        IDUNIDADEA: 15166,
        UNIDADEAMO: "AHR_UA_RWY_1028_6_11",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2907
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.724278876142606, 38.520088081824348],
              [-28.72417269155417, 38.520069891460224],
              [-28.724707846053853, 38.520068239776954],
              [-28.724745168072154, 38.520068124500192],
              [-28.724756306532232, 38.520068090094476],
              [-28.724766976042989, 38.520068057136299],
              [-28.724773946088895, 38.520248252510342],
              [-28.724765970382318, 38.520248346878766],
              [-28.724755162679148, 38.520248474754553],
              [-28.724720095331026, 38.520248889661318],
              [-28.72470898883504, 38.520249021067968],
              [-28.724531246145727, 38.520170100382835],
              [-28.724402026828173, 38.520122766335831],
              [-28.724278876142606, 38.520088081824348]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 498.309652615,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 618,
        IDUNIDADEA: 15167,
        UNIDADEAMO: "AHR_UA_RWY_1028_6_12",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2907
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.725074130391953, 38.520244700337976],
              [-28.725074548462491, 38.520067106625092],
              [-28.725082785280897, 38.520067081159205],
              [-28.72509068781568, 38.520067056726276],
              [-28.725100995706072, 38.52006702485221],
              [-28.725351229441436, 38.52006625082052],
              [-28.725364269015238, 38.520066210473374],
              [-28.725368448047579, 38.520241216839942],
              [-28.725352692703467, 38.520241403336492],
              [-28.725112427308851, 38.520244247103498],
              [-28.72510149112334, 38.520244376531814],
              [-28.725093206041848, 38.520244474583961],
              [-28.725087022370712, 38.520244547766026],
              [-28.725082229101886, 38.520244604492795],
              [-28.725074130391953, 38.520244700337976]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 457.40744047800001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 617,
        IDUNIDADEA: 15168,
        UNIDADEAMO: "AHR_UA_RWY_1028_5_13",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 94.38 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2908
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.706659790306272, 38.520320011129357],
              [-28.706663729034098, 38.52048676153985],
              [-28.706601632184125, 38.520483671228391],
              [-28.706513110237903, 38.520473007411375],
              [-28.706446123416228, 38.520447833583631],
              [-28.706389590513588, 38.520421005842316],
              [-28.706331866160397, 38.520383241647501],
              [-28.706278611720361, 38.520341910322223],
              [-28.706276047183565, 38.520339919956641],
              [-28.706269847575737, 38.520330569575897],
              [-28.706265194401293, 38.520323551557105],
              [-28.706263708068029, 38.520321309836639],
              [-28.706262009317886, 38.520318747744156],
              [-28.706659790306272, 38.520320011129357]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 455.700187175,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 617,
        IDUNIDADEA: 15169,
        UNIDADEAMO: "AHR_UA_RWY_1028_5_14",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2908
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.706663729034098, 38.52048676153985],
              [-28.706659790306272, 38.520320011129357],
              [-28.706663385868012, 38.520320022542968],
              [-28.707070750559712, 38.520321314952831],
              [-28.707069544568366, 38.520322151358585],
              [-28.707060993408977, 38.520328081946495],
              [-28.707054347810644, 38.520332690946752],
              [-28.707041771136311, 38.520341413393524],
              [-28.70702644851114, 38.520352040266836],
              [-28.70695521935998, 38.520412875368983],
              [-28.706905868023373, 38.520434450375014],
              [-28.706836798519316, 38.520461532345145],
              [-28.706796172967632, 38.520475814140354],
              [-28.70675510549535, 38.520485644068067],
              [-28.706684126889222, 38.520487776652104],
              [-28.70667014384431, 38.520487080777336],
              [-28.706663729034098, 38.52048676153985]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 455.81717585000001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 617,
        IDUNIDADEA: 15170,
        UNIDADEAMO: "AHR_UA_RWY_1028_5_15",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2908
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.706930637628215, 38.520320870588932],
              [-28.706927838313021, 38.520139215300908],
              [-28.706939712050772, 38.520139137148369],
              [-28.706952605024199, 38.520139052285927],
              [-28.706981519170171, 38.520138861965805],
              [-28.707309949088017, 38.520136699656902],
              [-28.707210911370954, 38.520206149758536],
              [-28.707097103702949, 38.520303037931441],
              [-28.707070750559712, 38.520321314952831],
              [-28.70698694410374, 38.520321049183387],
              [-28.706959219240979, 38.520320961248281],
              [-28.706939260311966, 38.52032089794038],
              [-28.706930637628215, 38.520320870588932]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 446.02758233700001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 617,
        IDUNIDADEA: 15171,
        UNIDADEAMO: "AHR_UA_RWY_1028_5_16",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 87.405 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2908
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.706674148840232, 38.520320056707916],
              [-28.706673439175788, 38.520140889457771],
              [-28.706680513410205, 38.520140842910912],
              [-28.706927838313021, 38.520139215300908],
              [-28.706930637628215, 38.520320870588932],
              [-28.706686229759384, 38.520320095055311],
              [-28.706674148840232, 38.520320056707916]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 338.90460998700001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 617,
        IDUNIDADEA: 15172,
        UNIDADEAMO: "AHR_UA_RWY_1028_5_17",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2908
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.706409593412289, 38.520142625197117],
              [-28.706413927230297, 38.520319230407551],
              [-28.706262009317886, 38.520318747744156],
              [-28.706243976455305, 38.520291550169567],
              [-28.706219122909271, 38.520260847323378],
              [-28.706203501261143, 38.520226876024971],
              [-28.706187186871563, 38.520173677483932],
              [-28.706187375559434, 38.520144086621151],
              [-28.706409593412289, 38.520142625197117]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 464.739200004,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15173,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_18",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 49.96 }],
        insp: "Sim",
        BERMA: 0,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.707484183214813, 38.520339724464279],
              [-28.707486301540424, 38.520135989902592],
              [-28.707494811358121, 38.520135955646978],
              [-28.707706750361151, 38.520135102302859],
              [-28.707720552429482, 38.520135046717378],
              [-28.707721616144386, 38.520338531063395],
              [-28.707715564064628, 38.520338561488728],
              [-28.707712908948981, 38.520338574836586],
              [-28.707496040304122, 38.520339664878854],
              [-28.707484183214813, 38.520339724464279]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 395.59566917900003,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15174,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_19",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2908
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.707283997124907, 38.520154898466195],
              [-28.707280537896949, 38.520340747829081],
              [-28.707163971485567, 38.520341333360996],
              [-28.707063320777493, 38.520421469151685],
              [-28.707021931299536, 38.520451359051044],
              [-28.70699241510221, 38.52046909077044],
              [-28.706958143094276, 38.520485768238451],
              [-28.706916437632373, 38.520506062955221],
              [-28.706902874542468, 38.520510619539657],
              [-28.706874807919764, 38.520446628967051],
              [-28.706905868023373, 38.520434450375014],
              [-28.706949309286237, 38.520415459087559],
              [-28.70695521935998, 38.520412875368983],
              [-28.70702644851114, 38.520352040266836],
              [-28.707097103702949, 38.520303037931441],
              [-28.707210911370954, 38.520206149758536],
              [-28.707283997124907, 38.520154898466195]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 455.544712672,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15175,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_20",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2908
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.706214667855072, 38.520380937919747],
              [-28.706295452812117, 38.520354980876739],
              [-28.706331866160397, 38.520383241647501],
              [-28.706389590513588, 38.520421005842316],
              [-28.706446123416228, 38.520447833583631],
              [-28.706513110237903, 38.520473007411375],
              [-28.706601632184125, 38.520483671228391],
              [-28.706663729034098, 38.52048676153985],
              [-28.706684126889222, 38.520487776652104],
              [-28.70675510549535, 38.520485644068067],
              [-28.706796172967632, 38.520475814140354],
              [-28.706836798519316, 38.520461532345145],
              [-28.706874807919764, 38.520446628967051],
              [-28.706902874542468, 38.520510619539657],
              [-28.706826991348244, 38.520536112819784],
              [-28.706783487605989, 38.520544404729122],
              [-28.706722656901476, 38.520552500981395],
              [-28.706664645308145, 38.520552688779276],
              [-28.706587549832879, 38.520552938311987],
              [-28.70651835623849, 38.520542052135028],
              [-28.706425781272642, 38.520517361294829],
              [-28.706388829240737, 38.520502040126054],
              [-28.706351921805329, 38.520483744997669],
              [-28.706313211849046, 38.52046187088262],
              [-28.706268761528925, 38.520430168877681],
              [-28.706216961016818, 38.520383524892324],
              [-28.706214667855072, 38.520380937919747]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 445.05074517100002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15176,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_21",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2908
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.706106616001101, 38.519873392517837],
              [-28.706135923280417, 38.519811491194588],
              [-28.706199723545808, 38.519862470496477],
              [-28.706189041294543, 38.519882860705252],
              [-28.706188667924, 38.519941361364189],
              [-28.706187186871563, 38.520173677483932],
              [-28.706203501261143, 38.520226876024971],
              [-28.706219122909271, 38.520260847323378],
              [-28.706243976455305, 38.520291550169567],
              [-28.706269847575737, 38.520330569575897],
              [-28.706276047183565, 38.520339919956641],
              [-28.706295452812117, 38.520354980876739],
              [-28.706214667855072, 38.520380937919747],
              [-28.706185788576821, 38.520348358469825],
              [-28.706175481219187, 38.520336730467839],
              [-28.706134452493949, 38.520270197118037],
              [-28.706103797897331, 38.520181523030672],
              [-28.706106008397651, 38.519939827443672],
              [-28.706106616001101, 38.519873392517837]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 461.89241447299997,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15177,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_22",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2908
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.70613840628743, 38.519806246710296],
              [-28.706212080256243, 38.519734887423965],
              [-28.706285137860235, 38.519690203258186],
              [-28.706367756252256, 38.519672964374422],
              [-28.70666802097702, 38.519667922611703],
              [-28.706757765451719, 38.519666415557367],
              [-28.706834045502696, 38.519666132792217],
              [-28.706835988141503, 38.519732439076463],
              [-28.706738091561228, 38.519732648436722],
              [-28.706669548184518, 38.519732580002035],
              [-28.706415761206202, 38.519732326268013],
              [-28.7063417145663, 38.519746897050261],
              [-28.706275258331541, 38.519778164595415],
              [-28.706212419086427, 38.519838237318908],
              [-28.706199723545808, 38.519862470496477],
              [-28.706135923280417, 38.519811491194588],
              [-28.70613840628743, 38.519806246710296]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 383.55970454499999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15178,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_23",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 47.22 }],
        insp: "Sim",
        BERMA: 0,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.707428584314766, 38.51966392717199],
              [-28.707430160521927, 38.51973062932403],
              [-28.70720980966319, 38.519731638300748],
              [-28.706835988141503, 38.519732439076463],
              [-28.706834045502696, 38.519666132792217],
              [-28.707428584314766, 38.51966392717199]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 450.32396459199998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15179,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_24",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.707430160521927, 38.51973062932403],
              [-28.707428584314766, 38.51966392717199],
              [-28.708005414291918, 38.519661784361723],
              [-28.708104665065662, 38.519661415377939],
              [-28.708129915170638, 38.5196613214924],
              [-28.708130212484772, 38.519727421155942],
              [-28.708106228035085, 38.519727531146046],
              [-28.70800859558787, 38.519727978827106],
              [-28.707430160521927, 38.51973062932403]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 455.31127914699999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15180,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_25",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.708856027118898, 38.519661036136618],
              [-28.708854244602218, 38.519724098510089],
              [-28.708831059809871, 38.519724204976512],
              [-28.708785969618265, 38.519724412021134],
              [-28.708130212484772, 38.519727421155942],
              [-28.708129915170638, 38.5196613214924],
              [-28.708311985613168, 38.519660644352506],
              [-28.708784469607611, 38.519660984749898],
              [-28.708830623827495, 38.519661017899082],
              [-28.708856027118898, 38.519661036136618]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 446.96901510599997,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15181,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_26",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.708854244602218, 38.519724098510089],
              [-28.708856027118898, 38.519661036136618],
              [-28.708859566819278, 38.51966103867742],
              [-28.709045219869378, 38.51966117178857],
              [-28.70947871919509, 38.519659106335681],
              [-28.709562675137061, 38.519658706133221],
              [-28.709572206289174, 38.51965866069618],
              [-28.709579828278869, 38.51965862435997],
              [-28.70958246679502, 38.519723785085382],
              [-28.709573747162096, 38.519723785772811],
              [-28.709564838097112, 38.519723786474522],
              [-28.709481224580006, 38.519723793026998],
              [-28.708911386028866, 38.519723836092446],
              [-28.708862606031623, 38.519724060112587],
              [-28.708858275566989, 38.519724079999079],
              [-28.708854244602218, 38.519724098510089]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 452.83990151699999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15182,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_27",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.710281803466799, 38.51965527571457],
              [-28.710281178381504, 38.519723727889421],
              [-28.70958246679502, 38.519723785085382],
              [-28.709579828278869, 38.51965862435997],
              [-28.710281803466799, 38.51965527571457]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 456.28171774499998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15183,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_28",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.71095386923243, 38.519652065802042],
              [-28.710955563380818, 38.519723611223057],
              [-28.710938770806703, 38.519723670249221],
              [-28.710297343210208, 38.519723726516837],
              [-28.710281178381504, 38.519723727889421],
              [-28.710281803466799, 38.51965527571457],
              [-28.710293703830413, 38.519655218909769],
              [-28.71095386923243, 38.519652065802042]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 475.34991701899997,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15184,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_29",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 30.24 }],
        insp: "Sim",
        BERMA: 0,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.711636788295859, 38.519648800099446],
              [-28.711637257974655, 38.51972121302088],
              [-28.710955563380818, 38.519723611223057],
              [-28.71095386923243, 38.519652065802042],
              [-28.711636788295859, 38.519648800099446]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 479.50257358300001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15185,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_30",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.711637257974655, 38.51972121302088],
              [-28.711636788295859, 38.519648800099446],
              [-28.712224591324194, 38.519645986048801],
              [-28.712315098482531, 38.5196455524922],
              [-28.712319207220961, 38.519718810288794],
              [-28.712224611722519, 38.519719143888594],
              [-28.711637257974655, 38.51972121302088]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 470.08054978799998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15186,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_31",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.712973606720926, 38.51964239593223],
              [-28.712978911997482, 38.519716481653127],
              [-28.712319207220961, 38.519718810288794],
              [-28.712315098482531, 38.5196455524922],
              [-28.712973606720926, 38.51964239593223]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 466.916605764,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15187,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_32",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 12.87 }],
        insp: "Sim",
        BERMA: 0,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.712945630496741, 38.520183477437051],
              [-28.712946755601731, 38.520113327683518],
              [-28.713000482828456, 38.520113144659611],
              [-28.713291382465606, 38.520112148113206],
              [-28.713634844328997, 38.52011132195365],
              [-28.71363539069262, 38.520181057145955],
              [-28.713004485022953, 38.520183268742045],
              [-28.712956685720286, 38.52018343630688],
              [-28.712945630496741, 38.520183477437051]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 464.44995702,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15188,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_33",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.712262784218925, 38.52018601589505],
              [-28.712263327889961, 38.520115663768891],
              [-28.712946755601731, 38.520113327683518],
              [-28.712945630496741, 38.520183477437051],
              [-28.712262784218925, 38.52018601589505]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 463.435410348,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15189,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_34",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.711584861719498, 38.520188532108399],
              [-28.711581907114613, 38.520117988952812],
              [-28.711635151383135, 38.520117807412774],
              [-28.712227704907438, 38.520115785425595],
              [-28.712263327889961, 38.520115663768891],
              [-28.712262784218925, 38.52018601589505],
              [-28.712224172440788, 38.520186159314022],
              [-28.711636822227149, 38.52018833938795],
              [-28.711584861719498, 38.520188532108399]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 457.76038708200002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15190,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_35",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.710913541418932, 38.520191019947532],
              [-28.710914367345506, 38.520120262920841],
              [-28.710983171475917, 38.520120028716157],
              [-28.711581907114613, 38.520117988952812],
              [-28.711584861719498, 38.520188532108399],
              [-28.71098642653012, 38.520190750029627],
              [-28.710913541418932, 38.520191019947532]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 469.71232171700001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15191,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_36",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.71022488020138, 38.52019294696867],
              [-28.7102232337697, 38.520123388120027],
              [-28.710267890830519, 38.520123149318671],
              [-28.710620700183938, 38.520121262089496],
              [-28.710914367345506, 38.520120262920841],
              [-28.710913541418932, 38.520191019947532],
              [-28.71086140424989, 38.520191213001013],
              [-28.710271110672512, 38.520192821147909],
              [-28.71022488020138, 38.52019294696867]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 463.533578955,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15192,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_37",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.709528327773405, 38.520194840494241],
              [-28.709524455330417, 38.520127122583204],
              [-28.709553938303657, 38.520126965102463],
              [-28.7102232337697, 38.520123388120027],
              [-28.71022488020138, 38.52019294696867],
              [-28.709555542510987, 38.520194766590819],
              [-28.709528327773405, 38.520194840494241]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 477.38830696600002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15193,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_38",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.708781613352578, 38.520195271808518],
              [-28.708781077231542, 38.520130770766237],
              [-28.708840649779173, 38.52013053028945],
              [-28.70902755900098, 38.520129775595315],
              [-28.709524455330417, 38.520127122583204],
              [-28.709528327773405, 38.520194840494241],
              [-28.709344705957804, 38.520195338965976],
              [-28.708849863962538, 38.520195280092743],
              [-28.708781613352578, 38.520195271808518]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 471.93249258600002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15194,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_39",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.708412997807528, 38.520335053252644],
              [-28.708410721682647, 38.520132265102028],
              [-28.708427248907434, 38.520132198441829],
              [-28.708458948545083, 38.520132070579358],
              [-28.708480781912463, 38.520131982845719],
              [-28.708744805806642, 38.520130917168409],
              [-28.708774805674729, 38.520130796080871],
              [-28.708781077231542, 38.520130770766237],
              [-28.708781613352578, 38.520195271808518],
              [-28.708774678126204, 38.5201952709645],
              [-28.708748302966715, 38.520195267750871],
              [-28.708738805996166, 38.520195266592275],
              [-28.708447085605812, 38.520334345537123],
              [-28.70844594903782, 38.52033488739832],
              [-28.708427880251978, 38.520334978345581],
              [-28.708412997807528, 38.520335053252644]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 451.61896947700001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15195,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_40",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.708183715441532, 38.520336207048906],
              [-28.708180069878168, 38.520133195159502],
              [-28.708410721682647, 38.520132265102028],
              [-28.708412997807528, 38.520335053252644],
              [-28.708183715441532, 38.520336207048906]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.85733181699999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15196,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_41",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.707955357450945, 38.520337355747124],
              [-28.707951821668484, 38.520134115077539],
              [-28.707964368182498, 38.520134064522338],
              [-28.708180069878168, 38.520133195159502],
              [-28.708183715441532, 38.520336207048906],
              [-28.707971853576243, 38.520337272782392],
              [-28.707955357450945, 38.520337355747124]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 457.63738843499999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15197,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_42",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.707721616144386, 38.520338531063395],
              [-28.707720552429482, 38.520135046717378],
              [-28.707951821668484, 38.520134115077539],
              [-28.707955357450945, 38.520337355747124],
              [-28.707721616144386, 38.520338531063395]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 397.82870780600001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 613,
        IDUNIDADEA: 15198,
        UNIDADEAMO: "AHR_UA_RWY_1028_1_43",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.707308202777433, 38.52013792385123],
              [-28.707309949088017, 38.520136699656902],
              [-28.707486301540424, 38.520135989902592],
              [-28.707484183214813, 38.520339724464279],
              [-28.70731368676017, 38.520340581211663],
              [-28.707280537896949, 38.520340747829081],
              [-28.707283997124907, 38.520154898466195],
              [-28.707308202777433, 38.52013792385123]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 447.53729606500002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 616,
        IDUNIDADEA: 15199,
        UNIDADEAMO: "AHR_UA_RWY_1028_4_44",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716743432072086, 38.519700470301828],
              [-28.716746202590855, 38.519624240581969],
              [-28.716787356478886, 38.519624041861348],
              [-28.71691457145171, 38.519623427484362],
              [-28.717358223293125, 38.519621283812889],
              [-28.717358809696108, 38.51969576376586],
              [-28.716915012432729, 38.519699158450699],
              [-28.716788457193676, 38.519700126077822],
              [-28.716743432072086, 38.519700470301828]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 456.52839229900002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 616,
        IDUNIDADEA: 15200,
        UNIDADEAMO: "AHR_UA_RWY_1028_4_45",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 25.168 }],
        insp: "Sim",
        BERMA: 0,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716133223636156, 38.519705133731705],
              [-28.716132395499042, 38.519627202767303],
              [-28.71665592234724, 38.519624676469306],
              [-28.716746202590855, 38.519624240581969],
              [-28.716743432072086, 38.519700470301828],
              [-28.716657738642429, 38.519701125393489],
              [-28.716133223636156, 38.519705133731705]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 471.119968331,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 616,
        IDUNIDADEA: 15201,
        UNIDADEAMO: "AHR_UA_RWY_1028_4_46",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715507912093521, 38.519707520805873],
              [-28.715504683429074, 38.519630228728012],
              [-28.715543170777288, 38.519630043292025],
              [-28.716089748032736, 38.519627408461041],
              [-28.716132395499042, 38.519627202767303],
              [-28.716133223636156, 38.519705133731705],
              [-28.716093267126027, 38.519705438982584],
              [-28.716093130334649, 38.519705439469419],
              [-28.715545007185757, 38.519707388945889],
              [-28.715507912093521, 38.519707520805873]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 460.94549013099999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 616,
        IDUNIDADEA: 15202,
        UNIDADEAMO: "AHR_UA_RWY_1028_4_47",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 29.9055 }],
        insp: "Sim",
        BERMA: 0,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.713631741023441, 38.519714173627371],
              [-28.713631155213939, 38.519639240276263],
              [-28.714219191659449, 38.519636415088399],
              [-28.714262743616786, 38.519636205727963],
              [-28.714264540260384, 38.519711933109647],
              [-28.714220986951482, 38.519712087426086],
              [-28.713631741023441, 38.519714173627371]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 469.48095486199998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 616,
        IDUNIDADEA: 15203,
        UNIDADEAMO: "AHR_UA_RWY_1028_4_48",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.714876464166156, 38.519709763250361],
              [-28.714874648526212, 38.519633262501287],
              [-28.714934236547425, 38.519632975715503],
              [-28.715504683429074, 38.519630228728012],
              [-28.715507912093521, 38.519707520805873],
              [-28.71494029566928, 38.519709536722019],
              [-28.714876464166156, 38.519709763250361]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 453.35821913400002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15204,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_49",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 26.88 }],
        insp: "Sim",
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.717362514020881, 38.520166455139858],
              [-28.717361990250673, 38.520099942657602],
              [-28.717830164670918, 38.520096956071797],
              [-28.717938995018852, 38.520096261551245],
              [-28.717998532741667, 38.520095881557701],
              [-28.718060125945986, 38.52009548841334],
              [-28.718059703049093, 38.5201632603086],
              [-28.717999139913072, 38.520163538000588],
              [-28.717939431166208, 38.520163811744418],
              [-28.717830532332741, 38.520164310929218],
              [-28.717362514020881, 38.520166455139858]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 459.29368011999998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15205,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_50",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.717358809696108, 38.51969576376586],
              [-28.717358223293125, 38.519621283812889],
              [-28.717827555875434, 38.51961901426202],
              [-28.717935910310896, 38.519618490025266],
              [-28.718002649495741, 38.51961816708009],
              [-28.718004377029722, 38.519690822983499],
              [-28.717936380675724, 38.519691343554257],
              [-28.717827955197862, 38.519692173563136],
              [-28.717358809696108, 38.51969576376586]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 468.99434760499997,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15206,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_51",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.718769093371876, 38.520090960799138],
              [-28.718767514812306, 38.520160012551138],
              [-28.718059703049093, 38.5201632603086],
              [-28.718060125945986, 38.52009548841334],
              [-28.718769093371876, 38.520090960799138]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 453.22856892099998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15207,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_52",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.719438266120719, 38.520086683382701],
              [-28.719443095370579, 38.520156908692655],
              [-28.718767514812306, 38.520160012551138],
              [-28.718769093371876, 38.520090960799138],
              [-28.719438266120719, 38.520086683382701]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 463.45535893700003,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15208,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_53",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.719443095370579, 38.520156908692655],
              [-28.719438266120719, 38.520086683382701],
              [-28.720057330858115, 38.520082722854852],
              [-28.720114755118438, 38.520082355311523],
              [-28.720114817706037, 38.520154178661841],
              [-28.720057836652234, 38.520154410254385],
              [-28.719446180399075, 38.520156894509974],
              [-28.719443095370579, 38.520156908692655]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 465.577917313,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15209,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_54",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.720775909898183, 38.520078121568467],
              [-28.720778919488243, 38.520151477455173],
              [-28.720114817706037, 38.520154178661841],
              [-28.720114755118438, 38.520082355311523],
              [-28.720775909898183, 38.520078121568467]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 460.99372172300002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15210,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_55",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.720778919488243, 38.520151477455173],
              [-28.720775909898183, 38.520078121568467],
              [-28.720800747675689, 38.520077962445527],
              [-28.721382201988703, 38.520074235859305],
              [-28.721419528380405, 38.520073996532858],
              [-28.721420517771485, 38.520148864202035],
              [-28.72138118440035, 38.520149024509472],
              [-28.720801058410053, 38.520151387341123],
              [-28.720778919488243, 38.520151477455173]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 457.57648121099999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15211,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_56",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.722052164438814, 38.520071999482681],
              [-28.722048731426501, 38.520146302057384],
              [-28.721420517771485, 38.520148864202035],
              [-28.721419528380405, 38.520073996532858],
              [-28.721672754486573, 38.520072372603295],
              [-28.722052164438814, 38.520071999482681]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 462.21105719000002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15212,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_57",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.722703194416884, 38.52007135637875],
              [-28.72270103190775, 38.520143638107506],
              [-28.722058861578997, 38.52014626071432],
              [-28.722048731426501, 38.520146302057384],
              [-28.722052164438814, 38.520071999482681],
              [-28.722703194416884, 38.52007135637875]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 452.52782372199999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15213,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_58",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 23.246 }],
        insp: "Sim",
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.72270103190775, 38.520143638107506],
              [-28.722703194416884, 38.52007135637875],
              [-28.722730170971353, 38.520071329652495],
              [-28.723274515494285, 38.520070789029923],
              [-28.72333825593007, 38.520070725559748],
              [-28.723358234857262, 38.520070705658348],
              [-28.723358269671991, 38.520140950317554],
              [-28.723339222373973, 38.520141028263893],
              [-28.723274157557618, 38.520141294502039],
              [-28.722730736181955, 38.520143516710775],
              [-28.72270103190775, 38.520143638107506]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 467.88436927700002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15214,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_59",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.724056105630059, 38.520070008356683],
              [-28.724058362774826, 38.520138083210462],
              [-28.723932682539797, 38.520138598219965],
              [-28.723358269671991, 38.520140950317554],
              [-28.723358234857262, 38.520070705658348],
              [-28.723928378166914, 38.520070136291103],
              [-28.724056105630059, 38.520070008356683]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 451.62465591799997,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15215,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_60",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 24.0 }],
        insp: "Sim",
        BERMA: 0,
        IDSG: 2907
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.724688189232992, 38.520239785629315],
              [-28.724685986479074, 38.520321293018803],
              [-28.724539039617039, 38.520251031693199],
              [-28.724474221400829, 38.520225211814576],
              [-28.724414157722098, 38.520201285823141],
              [-28.72429690657437, 38.520161763545239],
              [-28.724214519688395, 38.520148758985215],
              [-28.724168288812216, 38.520142829447984],
              [-28.72410716330231, 38.520137883200462],
              [-28.724066621224562, 38.520138049364476],
              [-28.724058362774826, 38.520138083210462],
              [-28.724056105630059, 38.520070008356683],
              [-28.724064315106027, 38.520070000129166],
              [-28.72417269155417, 38.520069891460224],
              [-28.724278876142606, 38.520088081824348],
              [-28.724402026828173, 38.520122766335831],
              [-28.724467666522891, 38.520146810695515],
              [-28.724531246145727, 38.520170100382835],
              [-28.724688189232992, 38.520239785629315]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 451.121291391,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15216,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_61",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2907
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.724940808712883, 38.520432363297758],
              [-28.724827887251926, 38.520382032444758],
              [-28.72470158792455, 38.520328589780938],
              [-28.724698610466572, 38.520327329050126],
              [-28.724685986655093, 38.520321293447637],
              [-28.724688188364855, 38.520239785348245],
              [-28.724700579642768, 38.520245289228022],
              [-28.724805654640694, 38.520291941924],
              [-28.724986793736772, 38.520375524286017],
              [-28.72504367930577, 38.52039578675361],
              [-28.725146346510204, 38.520415965247679],
              [-28.725196871647917, 38.520417336525995],
              [-28.725233942183422, 38.52041834231629],
              [-28.725307776591588, 38.520406050669152],
              [-28.725325110816918, 38.520473090529194],
              [-28.725191980233021, 38.520484484208495],
              [-28.725191300402905, 38.520484540976803],
              [-28.725098537081706, 38.520481040480703],
              [-28.725049288605476, 38.520469600778036],
              [-28.724940808712883, 38.520432363297758]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 457.55992600600001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15217,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_62",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2907
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.725674350958293, 38.520046051547119],
              [-28.725763556913289, 38.520044344849737],
              [-28.725763990827769, 38.520082222201388],
              [-28.725764062061376, 38.520088440330653],
              [-28.725742871016923, 38.520171294819171],
              [-28.725725969987085, 38.520208039652402],
              [-28.72567189962659, 38.520287682266201],
              [-28.725604783560545, 38.520354273192609],
              [-28.725523455261271, 38.520406925406469],
              [-28.72543471305265, 38.520446801475551],
              [-28.725346517563718, 38.520471258466877],
              [-28.725325110816918, 38.520473090529194],
              [-28.725307776591588, 38.520406050669152],
              [-28.725316301395935, 38.520404631491708],
              [-28.725382411272257, 38.52038791840836],
              [-28.725437803970394, 38.520367971406934],
              [-28.725525405573279, 38.520321907066574],
              [-28.725576492856305, 38.520279133078908],
              [-28.725638660491597, 38.52019937280064],
              [-28.725674312877295, 38.520113129469038],
              [-28.725674339838399, 38.520065250624704],
              [-28.725674350958293, 38.520046051547119]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 472.556383009,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15218,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_63",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2907
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.725416926212748, 38.519657044751192],
              [-28.72541705190671, 38.519588524366483],
              [-28.725452212079666, 38.519588377249612],
              [-28.725465401355724, 38.519590974527041],
              [-28.725511526826267, 38.519600057699414],
              [-28.725535841288167, 38.519604845771241],
              [-28.72558298357341, 38.519621299685539],
              [-28.725597028593857, 38.51962620176694],
              [-28.725661892163942, 38.519667055432564],
              [-28.725697618957991, 38.519702184636728],
              [-28.725718505587913, 38.519722721134819],
              [-28.725760680891547, 38.519793287905323],
              [-28.725763526958296, 38.520041825958948],
              [-28.725763557022681, 38.520044345058203],
              [-28.725674350406997, 38.520046051116744],
              [-28.725674355406987, 38.520038844414614],
              [-28.725674362900392, 38.5200257037434],
              [-28.725674472688151, 38.519835899889245],
              [-28.725658713567519, 38.519778698609166],
              [-28.725618082261004, 38.51972886543102],
              [-28.725614916317554, 38.5197265421548],
              [-28.725568032894646, 38.51969213837107],
              [-28.725520049310692, 38.519672074743625],
              [-28.725511603013448, 38.519668543045896],
              [-28.725461254509892, 38.519660548586643],
              [-28.725437652494545, 38.519656800992458],
              [-28.725416926212748, 38.519657044751192]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 463.20825953000002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15219,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_64",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2907
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.72474065971474, 38.519662945997673],
              [-28.724739261989761, 38.519591358304318],
              [-28.724770113309507, 38.519591229395743],
              [-28.724807929243802, 38.51959107137526],
              [-28.724923642775327, 38.519590587770139],
              [-28.72541705190671, 38.519588524366483],
              [-28.725416926212748, 38.519657044751192],
              [-28.724998803180512, 38.51966196144982],
              [-28.724921706165816, 38.519662255554067],
              [-28.724799616253868, 38.519662721190187],
              [-28.724777860518824, 38.519662804150549],
              [-28.72474065971474, 38.519662945997673]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 474.55843772999998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15220,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_65",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.72435221419375, 38.519664426433735],
              [-28.724350653962457, 38.519463121306032],
              [-28.724369311492811, 38.519463058920202],
              [-28.724388803462805, 38.519462993600825],
              [-28.724440005002471, 38.519462822801955],
              [-28.724513451458073, 38.519462577759107],
              [-28.72451677896964, 38.519592287682478],
              [-28.724739261989761, 38.519591358304318],
              [-28.72474065971474, 38.519662945997673],
              [-28.72444480805845, 38.51966407366276],
              [-28.724394201255414, 38.519664266503774],
              [-28.724375768988203, 38.51966433671268],
              [-28.72435221419375, 38.519664426433735]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 464.10317269900003,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15221,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_66",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.718672069365777, 38.519614926637857],
              [-28.718672083331114, 38.519685709634388],
              [-28.718062653574666, 38.519690376580549],
              [-28.71802343311785, 38.519690678261689],
              [-28.718004375488622, 38.519690824287828],
              [-28.71800264792855, 38.519618167286772],
              [-28.718017352846655, 38.519618097238428],
              [-28.718063106890639, 38.519617874574649],
              [-28.718672069365777, 38.519614926637857]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 452.77838083400002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15222,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_67",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.719339047920244, 38.519611692904085],
              [-28.719340120803924, 38.519681780874386],
              [-28.719067331438055, 38.51968268001491],
              [-28.718672083331114, 38.519685709634388],
              [-28.718672069365777, 38.519614926637857],
              [-28.719339047920244, 38.519611692904085]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 455.30034210999997,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15223,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_68",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.719340120803924, 38.519681780874386],
              [-28.719339047920244, 38.519611692904085],
              [-28.719974654659755, 38.519608607734696],
              [-28.720004528179459, 38.519608462646808],
              [-28.720007116887196, 38.519679579712133],
              [-28.720001770983171, 38.519679597369354],
              [-28.719975666191591, 38.519679683588357],
              [-28.719340120803924, 38.519681780874386]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 459.79181947500001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15224,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_69",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.720007116887196, 38.519679579712133],
              [-28.720004528179459, 38.519608462646808],
              [-28.720314280600284, 38.519606957810716],
              [-28.720459773118524, 38.519606979755636],
              [-28.720479875649843, 38.519606982773489],
              [-28.720527034606196, 38.519606989839694],
              [-28.720673665372455, 38.519607011689018],
              [-28.720673156725333, 38.519677377912956],
              [-28.720528300457786, 38.519677857102351],
              [-28.720482832023301, 38.519678007476479],
              [-28.72045867118106, 38.519678087374537],
              [-28.720007116887196, 38.519679579712133]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 450.466777154,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15225,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_70",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 23.98 }],
        insp: "Sim",
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.720673156725333, 38.519677377912956],
              [-28.720673665372455, 38.519607011689018],
              [-28.721180561255828, 38.519607085805994],
              [-28.721244442827093, 38.519607094990839],
              [-28.721276287048038, 38.519607099556332],
              [-28.721307924280872, 38.519607104083605],
              [-28.72134344694938, 38.519607109156645],
              [-28.721346398736738, 38.519607109577727],
              [-28.721345558358195, 38.519675151238502],
              [-28.721311512433349, 38.519675264075282],
              [-28.721275416574795, 38.519675383695194],
              [-28.721245314864589, 38.51967548344232],
              [-28.721180578343468, 38.519675697931504],
              [-28.720673156725333, 38.519677377912956]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 464.96399861200001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15226,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_71",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.721345558358195, 38.519675151238502],
              [-28.721346398736738, 38.519607109577727],
              [-28.722047033234567, 38.519607207415532],
              [-28.722064411870225, 38.519607209788965],
              [-28.722065974169343, 38.51967276127769],
              [-28.722048996787549, 38.519672817650701],
              [-28.721345558358195, 38.519675151238502]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 477.52066340300001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15227,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_72",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.722065974169343, 38.51967276127769],
              [-28.722064411870225, 38.519607209788965],
              [-28.722750016615031, 38.519607301365937],
              [-28.72279774600268, 38.51960730749061],
              [-28.722833414020442, 38.519607312052401],
              [-28.722833383926609, 38.519670203883763],
              [-28.722822446128042, 38.519670245425218],
              [-28.722822236798812, 38.519670245205809],
              [-28.722822166115499, 38.519670246246022],
              [-28.72281724358016, 38.519670264237867],
              [-28.722796842551123, 38.51967033224004],
              [-28.722752560137959, 38.519670479419858],
              [-28.722065974169343, 38.51967276127769]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 472.08886381100001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15228,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_73",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.722833383926609, 38.519670203883763],
              [-28.722833414020442, 38.519607312052401],
              [-28.723110325737281, 38.519607347882847],
              [-28.723126105154023, 38.5195996994468],
              [-28.723341443921409, 38.519499926291765],
              [-28.723351561855061, 38.519495238081056],
              [-28.723369214891029, 38.519487058428005],
              [-28.723369518263816, 38.51966816566766],
              [-28.723348958237441, 38.519668243812156],
              [-28.723337687162644, 38.519668286649697],
              [-28.722981822436488, 38.519669638971855],
              [-28.722964247139217, 38.519669706789529],
              [-28.722833383926609, 38.519670203883763]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 469.60561955999998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15229,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_74",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.724113534389698, 38.51966533538279],
              [-28.724107420077484, 38.519463932284722],
              [-28.724123155121632, 38.519463879845773],
              [-28.724129964194805, 38.519463857153049],
              [-28.724335646277691, 38.519463171485611],
              [-28.724350653962457, 38.519463121306032],
              [-28.72435221419375, 38.519664426433735],
              [-28.724334627129387, 38.519664493420365],
              [-28.724140652968536, 38.519665232132603],
              [-28.724131458233675, 38.519665267140951],
              [-28.724113534389698, 38.51966533538279]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 481.33234612799998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15230,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_75",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.723864876239009, 38.519666281820442],
              [-28.723862414188012, 38.519464748524207],
              [-28.723868114344349, 38.519464729539912],
              [-28.723871974468111, 38.51946471668365],
              [-28.723923482456335, 38.519464545122695],
              [-28.724092030599174, 38.51946398357002],
              [-28.724107420077484, 38.519463932284722],
              [-28.724113534389698, 38.51966533538279],
              [-28.724095301148164, 38.519665404799738],
              [-28.723920399572695, 38.519666070534413],
              [-28.723878470517217, 38.519666230091822],
              [-28.723864876239009, 38.519666281820442]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 482.75251497800002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15231,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_76",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 20.26 }],
        insp: "Sim",
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.723618633943861, 38.519667218542345],
              [-28.72361382385084, 38.519465576181155],
              [-28.723614779898384, 38.519465572999096],
              [-28.72362385214916, 38.519465542803147],
              [-28.723726957511236, 38.51946519957928],
              [-28.723846449759449, 38.519464801692074],
              [-28.723853110593808, 38.519464779509129],
              [-28.723862414188012, 38.519464748524207],
              [-28.723864876239009, 38.519666281820442],
              [-28.723852528212912, 38.519666328805471],
              [-28.72374209057001, 38.519666748969925],
              [-28.723628662156621, 38.519667180404468],
              [-28.7236231399568, 38.519667201405824],
              [-28.723618633943861, 38.519667218542345]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 477.48844840300001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 614,
        IDUNIDADEA: 15232,
        UNIDADEAMO: "AHR_UA_RWY_1028_2_77",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.723381207918564, 38.519481501375402],
              [-28.723414143107945, 38.519466240616076],
              [-28.723514347566987, 38.519465907229701],
              [-28.723558867332724, 38.519465759082181],
              [-28.723571705940714, 38.519465716356258],
              [-28.723583451794202, 38.519465677265714],
              [-28.72361382385084, 38.519465576181155],
              [-28.723618633943861, 38.519667218542345],
              [-28.72358575759797, 38.519667343566901],
              [-28.723579067322461, 38.519667369008047],
              [-28.723564068113053, 38.519667426044229],
              [-28.723556065096115, 38.519667456475801],
              [-28.723520177583669, 38.519667592931846],
              [-28.723383294955944, 38.519668113303211],
              [-28.723369518263816, 38.51966816566766],
              [-28.723369214891029, 38.519487058428005],
              [-28.723381207918564, 38.519481501375402]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 573.99433911599999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15233,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_78",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2908
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.706476945930788, 38.519836186744151],
              [-28.706478542727936, 38.520043063251542],
              [-28.706188005891971, 38.520044974436622],
              [-28.706189041294543, 38.519882860705252],
              [-28.706212419086427, 38.519838237318908],
              [-28.706212921618242, 38.519837757545112],
              [-28.706476945930788, 38.519836186744151]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 520.13190709100002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15234,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_79",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2907
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.725411581127052, 38.51996782711646],
              [-28.725409927381772, 38.519760609924013],
              [-28.725643147086679, 38.519759608274619],
              [-28.725658713567519, 38.519778698609166],
              [-28.725674472688151, 38.519835899889245],
              [-28.725674396519032, 38.519966698323834],
              [-28.725411581127052, 38.51996782711646]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 557.21917846400004,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15235,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_80",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.717073693158145, 38.519898110029928],
              [-28.717072118994519, 38.519697957384601],
              [-28.717358809696108, 38.51969576376586],
              [-28.717359580044153, 38.519793623123647],
              [-28.717360395306525, 38.51989723040964],
              [-28.717073693158145, 38.519898110029928]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 464.644338092,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15236,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_81",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.719194465136098, 38.519787135033916],
              [-28.719193636926505, 38.519682263778662],
              [-28.719652358503897, 38.519680750927961],
              [-28.719653183953685, 38.519785188758263],
              [-28.719194465136098, 38.519787135033916]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 418.97668445,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15237,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_82",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.719196101631638, 38.519994351691679],
              [-28.719196843021383, 38.520088226761217],
              [-28.718738130537915, 38.520091158313136],
              [-28.718737381293025, 38.519996296168671],
              [-28.719196101631638, 38.519994351691679]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.497918814,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15238,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_83",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.717583214397123, 38.519793955206225],
              [-28.717584846336411, 38.520001172497189],
              [-28.71736122069068, 38.520002117392941],
              [-28.717359590105559, 38.519794900623523],
              [-28.717583214397123, 38.519793955206225]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50016404299998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15239,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_84",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.717806840163338, 38.519793009723749],
              [-28.717808472102394, 38.520000227014584],
              [-28.717584846336411, 38.520001172497189],
              [-28.717583214397123, 38.519793955206225],
              [-28.717806840163338, 38.519793009723749]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50049301799999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15240,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_85",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.718030465734486, 38.519792063814876],
              [-28.718032098314922, 38.519999281102869],
              [-28.717808472102394, 38.520000227014584],
              [-28.717806840163338, 38.519793009723749],
              [-28.718030465734486, 38.519792063814876]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50052580099998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15241,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_86",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 63.17 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.718254091283701, 38.519791117478896],
              [-28.718255724505518, 38.519998334764018],
              [-28.718032098314922, 38.519999281102869],
              [-28.718030465734486, 38.519792063814876],
              [-28.718254091283701, 38.519791117478896]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50055858500002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15242,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_87",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.718477716810956, 38.51979017071573],
              [-28.718479350674155, 38.519997387998011],
              [-28.718255724505518, 38.519998334764018],
              [-28.718254091283701, 38.519791117478896],
              [-28.718477716810956, 38.51979017071573]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50059136700003,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15243,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_88",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.718701342316294, 38.5197892235254],
              [-28.718702976820836, 38.519996440804881],
              [-28.718479350674155, 38.519997387998011],
              [-28.718477716810956, 38.51979017071573],
              [-28.718701342316294, 38.5197892235254]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50062415000002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15244,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_89",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.718924967799637, 38.519788275907992],
              [-28.718926602945572, 38.519995493184524],
              [-28.718702976820836, 38.519996440804881],
              [-28.718701342316294, 38.5197892235254],
              [-28.718924967799637, 38.519788275907992]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50065693200003,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15245,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_90",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.719148593261032, 38.519787327863391],
              [-28.719150229048317, 38.519994545137116],
              [-28.718926602945572, 38.519995493184524],
              [-28.718924967799637, 38.519788275907992],
              [-28.719148593261032, 38.519787327863391]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50068971399998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15246,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_91",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 69.83 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.719372218700453, 38.519786379391675],
              [-28.719373855129131, 38.519993596662495],
              [-28.719150229048317, 38.519994545137116],
              [-28.719148593261032, 38.519787327863391],
              [-28.719372218700453, 38.519786379391675]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50072249700003,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15247,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_92",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.719595844117904, 38.519785430492753],
              [-28.719597481187961, 38.519992647760773],
              [-28.719373855129131, 38.519993596662495],
              [-28.719372218700453, 38.519786379391675],
              [-28.719595844117904, 38.519785430492753]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50075528000002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15248,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_93",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.719819469513386, 38.519784481166752],
              [-28.719821107224806, 38.519991698431873],
              [-28.719597481187961, 38.519992647760773],
              [-28.719595844117904, 38.519785430492753],
              [-28.719819469513386, 38.519784481166752]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50078806300002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15249,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_94",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.720043094886872, 38.519783531413609],
              [-28.720044733239678, 38.519990748675824],
              [-28.719821107224806, 38.519991698431873],
              [-28.719819469513386, 38.519784481166752],
              [-28.720043094886872, 38.519783531413609]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50082084600001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15250,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_95",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.720266720238378, 38.519782581233272],
              [-28.720268359232563, 38.519989798492652],
              [-28.720044733239678, 38.519990748675824],
              [-28.720043094886872, 38.519783531413609],
              [-28.720266720238378, 38.519782581233272]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50085362800002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15251,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_96",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.720490345567899, 38.519781630625808],
              [-28.720491985203456, 38.519988847882303],
              [-28.720268359232563, 38.519989798492652],
              [-28.720266720238378, 38.519782581233272],
              [-28.720490345567899, 38.519781630625808]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 447.881975146,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15252,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_97",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 59.832 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.720713662282471, 38.519780680903885],
              [-28.720715302558517, 38.51998789815751],
              [-28.720491985203456, 38.519988847882303],
              [-28.720490345567899, 38.519781630625808],
              [-28.720713662282471, 38.519780680903885]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50091915799999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15253,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_98",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.720937287568031, 38.51977972944276],
              [-28.720938928485442, 38.519986946693479],
              [-28.720715302558517, 38.51998789815751],
              [-28.720713662282471, 38.519780680903885],
              [-28.720937287568031, 38.51977972944276]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50095194099998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15254,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_99",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.721160912831579, 38.51977877755445],
              [-28.721162554390364, 38.51998599480229],
              [-28.720938928485442, 38.519986946693479],
              [-28.720937287568031, 38.51977972944276],
              [-28.721160912831579, 38.51977877755445]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50098472399998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15255,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_100",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.721384538073121, 38.519777825238997],
              [-28.721386180273264, 38.519985042483967],
              [-28.721162554390364, 38.51998599480229],
              [-28.721160912831579, 38.51977877755445],
              [-28.721384538073121, 38.519777825238997]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50101750699997,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15256,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_101",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.72160816329264, 38.519776872496443],
              [-28.721609806134179, 38.519984089738472],
              [-28.721386180273264, 38.519985042483967],
              [-28.721384538073121, 38.519777825238997],
              [-28.72160816329264, 38.519776872496443]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50105028899998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15257,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_102",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.72183178849015, 38.519775919326698],
              [-28.721833431973057, 38.519983136565891],
              [-28.721609806134179, 38.519984089738472],
              [-28.72160816329264, 38.519776872496443],
              [-28.72183178849015, 38.519775919326698]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50108307199997,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15258,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_103",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.72205541366565, 38.51977496572983],
              [-28.722057057789915, 38.519982182966082],
              [-28.721833431973057, 38.519983136565891],
              [-28.72183178849015, 38.519775919326698],
              [-28.72205541366565, 38.51977496572983]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50111585399998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15259,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_104",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 66.19 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.722279038819089, 38.519774011705849],
              [-28.722280683584739, 38.51998122893918],
              [-28.722057057789915, 38.519982182966082],
              [-28.72205541366565, 38.51977496572983],
              [-28.722279038819089, 38.519774011705849]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50114863800002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15260,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_105",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.722502663950504, 38.51977305725466],
              [-28.722504309357522, 38.519980274485114],
              [-28.722280683584739, 38.51998122893918],
              [-28.722279038819089, 38.519774011705849],
              [-28.722502663950504, 38.51977305725466]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50118142899998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15261,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_106",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.722726289059896, 38.519772102376386],
              [-28.722727935108288, 38.51997931960392],
              [-28.722504309357522, 38.519980274485114],
              [-28.722502663950504, 38.51977305725466],
              [-28.722726289059896, 38.519772102376386]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50121422000001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15262,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_107",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.722949914147247, 38.519771147070934],
              [-28.722951560836993, 38.519978364295568],
              [-28.722727935108288, 38.51997931960392],
              [-28.722726289059896, 38.519772102376386],
              [-28.722949914147247, 38.519771147070934]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.501247014,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15263,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_108",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.72317353921251, 38.519770191338338],
              [-28.72317518654366, 38.519977408560088],
              [-28.722951560836993, 38.519978364295568],
              [-28.722949914147247, 38.519771147070934],
              [-28.72317353921251, 38.519770191338338]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50127980500002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15264,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_109",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 98.4 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.723397164255775, 38.519769235178629],
              [-28.723398812228275, 38.519976452397437],
              [-28.72317518654366, 38.519977408560088],
              [-28.72317353921251, 38.519770191338338],
              [-28.723397164255775, 38.519769235178629]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50131259599999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15265,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_110",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.723620789276943, 38.519768278591762],
              [-28.723622437890828, 38.519975495807657],
              [-28.723398812228275, 38.519976452397437],
              [-28.723397164255775, 38.519769235178629],
              [-28.723620789276943, 38.519768278591762]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.77958189100002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15266,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_111",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.723844553006156, 38.519767320983931],
              [-28.723846202261807, 38.519974538196884],
              [-28.723622437890828, 38.519975495807657],
              [-28.723620789276943, 38.519768278591762],
              [-28.723844553006156, 38.519767320983931]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50137821700002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15267,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_112",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.724068177983202, 38.519766363542487],
              [-28.724069827880218, 38.519973580752541],
              [-28.723846202261807, 38.519974538196884],
              [-28.723844553006156, 38.519767320983931],
              [-28.724068177983202, 38.519766363542487]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50141100899998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15268,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_113",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.724291802938161, 38.519765405673944],
              [-28.724293453476562, 38.519972622881042],
              [-28.724069827880218, 38.519973580752541],
              [-28.724068177983202, 38.519766363542487],
              [-28.724291802938161, 38.519765405673944]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50144380199998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15269,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_114",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.724515427871051, 38.519764447378222],
              [-28.724517079050834, 38.519971664582449],
              [-28.724293453476562, 38.519972622881042],
              [-28.724291802938161, 38.519765405673944],
              [-28.724515427871051, 38.519764447378222]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50147660200003,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15270,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_115",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.724739052781871, 38.519763488655393],
              [-28.724740704603025, 38.51997070585665],
              [-28.724517079050834, 38.519971664582449],
              [-28.724515427871051, 38.519764447378222],
              [-28.724739052781871, 38.519763488655393]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.501509403,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15271,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_116",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 35.798 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.724962677670604, 38.519762529505428],
              [-28.724964330133115, 38.519969746703758],
              [-28.724740704603025, 38.51997070585665],
              [-28.724739052781871, 38.519763488655393],
              [-28.724962677670604, 38.519762529505428]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50154220399997,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15272,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_117",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 74.86 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.725186302537242, 38.519761569928264],
              [-28.725187955641122, 38.519968787123652],
              [-28.724964330133115, 38.519969746703758],
              [-28.724962677670604, 38.519762529505428],
              [-28.725186302537242, 38.519761569928264]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.50157500400002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 619,
        IDUNIDADEA: 15273,
        UNIDADEAMO: "AHR_UA_RWY_1028_7_118",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2907
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.725409927381772, 38.519760609924013],
              [-28.725411581127052, 38.51996782711646],
              [-28.725187955641122, 38.519968787123652],
              [-28.725186302537242, 38.519761569928264],
              [-28.725409927381772, 38.519760609924013]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 457.39194367599998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15274,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_119",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.713410799267137, 38.520013119292422],
              [-28.71340914626094, 38.519801585756078],
              [-28.713632414277718, 38.519800399555812],
              [-28.713634067246343, 38.520011903934076],
              [-28.713615320534402, 38.520012293844381],
              [-28.713410799267137, 38.520013119292422]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 457.53187061599999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15275,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_120",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.713187173692216, 38.520014021886787],
              [-28.713185523509651, 38.519802772196833],
              [-28.71340914626094, 38.519801585756078],
              [-28.713410799267137, 38.520013119292422],
              [-28.713187173692216, 38.520014021886787]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 456.920170708,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15276,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_121",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.712963546753667, 38.520014924059417],
              [-28.712961899441396, 38.519803958217373],
              [-28.713185523509651, 38.519802772196833],
              [-28.713187173692216, 38.520014021886787],
              [-28.712963546753667, 38.520014924059417]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 456.30577277499998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15277,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_122",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.712739919793524, 38.520015825804883],
              [-28.712738275349743, 38.519805143810807],
              [-28.712961899441396, 38.519803958217373],
              [-28.712963546753667, 38.520014924059417],
              [-28.712739919793524, 38.520015825804883]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 455.69137484399999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15278,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_123",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.712516292811749, 38.52001672712322],
              [-28.712514651234716, 38.519806328977147],
              [-28.712738275349743, 38.519805143810807],
              [-28.712739919793524, 38.520015825804883],
              [-28.712516292811749, 38.52001672712322]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 455.23842323999997,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15279,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_124",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 68.61 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.712292569958585, 38.52001762840041],
              [-28.71229096416867, 38.519807514049688],
              [-28.712514651234716, 38.519806328977147],
              [-28.712516292811749, 38.52001672712322],
              [-28.712292569958585, 38.52001762840041]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 454.58287661600002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15280,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_125",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.712068900051989, 38.520018529036854],
              [-28.71206726420499, 38.51980869876305],
              [-28.71229096416867, 38.519807514049688],
              [-28.712292569958585, 38.52001762840041],
              [-28.712068900051989, 38.520018529036854]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 453.84779989700002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15281,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_126",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.711845273005373, 38.520019429073464],
              [-28.71184364001985, 38.519809882647813],
              [-28.71206726420499, 38.51980869876305],
              [-28.712068900051989, 38.520018529036854],
              [-28.711845273005373, 38.520019429073464]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 453.233408688,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15282,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_127",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.711621645937264, 38.52002032869548],
              [-28.711620015811373, 38.519811066105461],
              [-28.71184364001985, 38.519809882647813],
              [-28.711845273005373, 38.520019429073464],
              [-28.711731765763528, 38.520019885745029],
              [-28.711621645937264, 38.52002032869548]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 452.61905902799998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15283,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_128",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.71139801884765, 38.520021227903349],
              [-28.71139639157953, 38.51981224913601],
              [-28.711620015811373, 38.519811066105461],
              [-28.711621645937264, 38.52002032869548],
              [-28.71139801884765, 38.520021227903349]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 452.00471650100002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15284,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_129",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.711174391736453, 38.520022126684054],
              [-28.711172767324349, 38.519813431739422],
              [-28.71139639157953, 38.51981224913601],
              [-28.71139801884765, 38.520021227903349],
              [-28.711174391736453, 38.520022126684054]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 451.39037397200002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15285,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_130",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 67.14 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.710950764603659, 38.520023025037638],
              [-28.710949143045831, 38.519814613915806],
              [-28.711172767324349, 38.519813431739422],
              [-28.711174391736453, 38.520022126684054],
              [-28.710950764603659, 38.520023025037638]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 450.77603144599999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15286,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_131",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.710727137449286, 38.520023922964043],
              [-28.710725518743963, 38.519815795665032],
              [-28.710949143045831, 38.519814613915806],
              [-28.710950764603659, 38.520023025037638],
              [-28.710727137449286, 38.520023922964043]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 450.16168891799998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15287,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_132",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.710503510273352, 38.520024820463298],
              [-28.710501894418766, 38.51981697698713],
              [-28.710725518743963, 38.519815795665032],
              [-28.710727137449286, 38.520023922964043],
              [-28.710503510273352, 38.520024820463298]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 449.54734638999997,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15288,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_133",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.710279883075845, 38.520025717535354],
              [-28.710278270070248, 38.519818157882163],
              [-28.710501894418766, 38.51981697698713],
              [-28.710503510273352, 38.520024820463298],
              [-28.710279883075845, 38.520025717535354]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.93300386099997,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15289,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_134",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.71005625585677, 38.520026614180289],
              [-28.710054645698399, 38.519819338350061],
              [-28.710278270070248, 38.519818157882163],
              [-28.710279883075845, 38.520025717535354],
              [-28.71005625585677, 38.520026614180289]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.51113425599999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15290,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_135",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.70983262861613, 38.52002751039808],
              [-28.709831019558944, 38.519820293756752],
              [-28.710008076029425, 38.519819584128214],
              [-28.710054645698399, 38.519819338350061],
              [-28.71005625585677, 38.520026614180289],
              [-28.70983262861613, 38.52002751039808]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.49755327399998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15291,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_136",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 67.08 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.709609001353932, 38.520028406188665],
              [-28.709607392938974, 38.519821189652113],
              [-28.709831019558944, 38.519820293756752],
              [-28.70983262861613, 38.52002751039808],
              [-28.709609001353932, 38.520028406188665]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.532099286,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15292,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_137",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.70938537434105, 38.520029336461718],
              [-28.709383766297432, 38.519822085120289],
              [-28.709607392938974, 38.519821189652113],
              [-28.709609001353932, 38.520028406188665],
              [-28.709591435005883, 38.520028476536631],
              [-28.70938537434105, 38.520029336461718]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.61205095700001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15293,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_138",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.709161748246842, 38.520030269279808],
              [-28.709160140241377, 38.519822980158899],
              [-28.709383766297432, 38.519822085120289],
              [-28.70938537434105, 38.520029336461718],
              [-28.709161748246842, 38.520030269279808]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.077607237,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15294,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_139",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.708938428891503, 38.520031200391969],
              [-28.708936821544601, 38.519823873540972],
              [-28.709160140241377, 38.519822980158899],
              [-28.709161748246842, 38.520030269279808],
              [-28.708938428891503, 38.520031200391969]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.77693494900001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15295,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_140",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.708714801836408, 38.520032132360143],
              [-28.708713194838484, 38.519824767728295],
              [-28.708936821544601, 38.519823873540972],
              [-28.708938428891503, 38.520031200391969],
              [-28.708714801836408, 38.520032132360143]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.858668749,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15296,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_141",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 63.03 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.708491174759565, 38.520033063901153],
              [-28.70848956811081, 38.519825661488461],
              [-28.708713194838484, 38.519824767728295],
              [-28.708714801836408, 38.520032132360143],
              [-28.708491174759565, 38.520033063901153]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.940402554,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15297,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_142",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.708267547660945, 38.520033995015041],
              [-28.708265941361617, 38.519826554821464],
              [-28.70848956811081, 38.519825661488461],
              [-28.708491174759565, 38.520033063901153],
              [-28.708267547660945, 38.520033995015041]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 449.02213636099998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15298,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_143",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.708043920540568, 38.520034925701758],
              [-28.708042314590898, 38.51982744772728],
              [-28.708265941361617, 38.519826554821464],
              [-28.708267547660945, 38.520033995015041],
              [-28.708043920540568, 38.520034925701758]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 449.10387016700003,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15299,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_144",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.707820293398441, 38.520035855961325],
              [-28.707818687798671, 38.519828340205983],
              [-28.708042314590898, 38.51982744772728],
              [-28.708043920540568, 38.520034925701758],
              [-28.707820293398441, 38.520035855961325]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.971034062,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15300,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_145",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.707596666234586, 38.520036785793707],
              [-28.707595063271853, 38.519829527953341],
              [-28.707745009564938, 38.519828634157051],
              [-28.707818687798671, 38.519828340205983],
              [-28.707820293398441, 38.520035855961325],
              [-28.707596666234586, 38.520036785793707]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.15010266299998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15301,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_146",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 99.85 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.707373039048957, 38.520037715199024],
              [-28.707371439844938, 38.519830860565712],
              [-28.707595063271853, 38.519829527953341],
              [-28.707596666234586, 38.520036785793707],
              [-28.707373039048957, 38.520037715199024]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 447.27738854699999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15302,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_147",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.707149411866826, 38.520038647441233],
              [-28.707147816393789, 38.519832192750982],
              [-28.707371439844938, 38.519830860565712],
              [-28.707373039048957, 38.520037715199024],
              [-28.707150754111588, 38.520038638602422],
              [-28.707149411866826, 38.520038647441233]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 443.95002142599998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15303,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_148",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.706927624743315, 38.520040107718991],
              [-28.706925407848747, 38.519833517274989],
              [-28.707147816393789, 38.519832192750982],
              [-28.707149411866826, 38.520038647441233],
              [-28.706927624743315, 38.520040107718991]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 450.35468106399998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15304,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_149",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 41.59 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.706702165785373, 38.520041591741908],
              [-28.706700569418828, 38.519834855840337],
              [-28.706925407848747, 38.519833517274989],
              [-28.706927624743315, 38.520040107718991],
              [-28.706702165785373, 38.520041591741908]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 447.60872461899999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 612,
        IDUNIDADEA: 15305,
        UNIDADEAMO: "AHR_UA_RWY_1028_8_150",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 92.47 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2908
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.706478542727936, 38.520043063251542],
              [-28.706476945930788, 38.519836186744151],
              [-28.706700569418828, 38.519834855840337],
              [-28.706702165785373, 38.520041591741908],
              [-28.706478542727936, 38.520043063251542]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 450.15816320099998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15306,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_151",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.713862621439635, 38.519907913822344],
              [-28.713864207888612, 38.520110770425703],
              [-28.713634844328997, 38.52011132195365],
              [-28.713634069819168, 38.52001221904591],
              [-28.713633259840801, 38.519908609412774],
              [-28.713862621439635, 38.519907913822344]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 431.75094226700003,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15307,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_152",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.71386109901345, 38.519713362912427],
              [-28.713862621439635, 38.519907913822344],
              [-28.713633259840801, 38.519908609412774],
              [-28.713632449831209, 38.519804998128393],
              [-28.713631740770424, 38.519714173945317],
              [-28.71386109901345, 38.519713362912427]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 450.48048735100002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15308,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_153",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.714091984653045, 38.519907216469981],
              [-28.714093572862836, 38.5201102165219],
              [-28.713864207888612, 38.520110770425703],
              [-28.713862621439635, 38.519907913822344],
              [-28.714091984653045, 38.519907216469981]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 432.01191663999998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15309,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_154",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.714090461676243, 38.519712550644492],
              [-28.714091984653045, 38.519907216469981],
              [-28.713862621439635, 38.519907913822344],
              [-28.71386109901345, 38.519713362912427],
              [-28.714090461676243, 38.519712550644492]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 450.79715708200001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15310,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_155",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 69.24 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.714321346973783, 38.519906518670886],
              [-28.714322936950055, 38.520109662072805],
              [-28.714103895647657, 38.520110191582397],
              [-28.714093572862836, 38.5201102165219],
              [-28.714091984653045, 38.519907216469981],
              [-28.714321346973783, 38.519906518670886]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 432.26411502500002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15311,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_156",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.714319822482363, 38.519711738293267],
              [-28.714321346973783, 38.519906518670886],
              [-28.714091984653045, 38.519907216469981],
              [-28.714090461676243, 38.519712550644492],
              [-28.714100783948961, 38.519712514078513],
              [-28.714319822482363, 38.519711738293267]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 451.11563824199999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15312,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_157",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.714550709273176, 38.519905820422395],
              [-28.714552301020166, 38.520109107598657],
              [-28.714441621188382, 38.52010937499476],
              [-28.714322936950055, 38.520109662072805],
              [-28.714321346973783, 38.519906518670886],
              [-28.714550709273176, 38.519905820422395]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 432.51840993100001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15313,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_158",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.714549183266588, 38.519710925509614],
              [-28.714550709273176, 38.519905820422395],
              [-28.714321346973783, 38.519906518670886],
              [-28.714319822482363, 38.519711738293267],
              [-28.714549183266588, 38.519710925509614]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 451.43539978899997,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15314,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_159",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.714780071551242, 38.519905121724534],
              [-28.714781665071953, 38.52010855293814],
              [-28.714572944742333, 38.520109057713029],
              [-28.714552301020166, 38.520109107598657],
              [-28.714550709273176, 38.519905820422395],
              [-28.714780071551242, 38.519905121724534]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 432.77317745,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15315,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_160",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 70.94 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.714778544025084, 38.519710111807861],
              [-28.714780071551242, 38.519905121724534],
              [-28.714550709273176, 38.519905820422395],
              [-28.714549183266588, 38.519710925509614],
              [-28.714569827035309, 38.519710852332437],
              [-28.714778544025084, 38.519710111807861]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 451.75514592399998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15316,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_161",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715009433807996, 38.519904422577255],
              [-28.715011029103145, 38.520107997808744],
              [-28.714781665071953, 38.52010855293814],
              [-28.714780071551242, 38.519905121724534],
              [-28.715009433807996, 38.519904422577255]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 433.02861038899999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15317,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_162",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715007904761158, 38.519709297610341],
              [-28.715009433807996, 38.519904422577255],
              [-28.714780071551242, 38.519905121724534],
              [-28.714778544025084, 38.519710111807861],
              [-28.715007904761158, 38.519709297610341]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 452.07460723499997,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15318,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_163",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 50.92 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715238796043412, 38.519903722980644],
              [-28.715240393111632, 38.520107441934513],
              [-28.715042507605176, 38.52010792158628],
              [-28.715011029103145, 38.520107997808744],
              [-28.715009433807996, 38.519904422577255],
              [-28.715238796043412, 38.519903722980644]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 433.28407600999998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15319,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_164",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715237265474936, 38.519708482934256],
              [-28.715238796043412, 38.519903722980644],
              [-28.715009433807996, 38.519904422577255],
              [-28.715007904761158, 38.519709297610341],
              [-28.715039113612189, 38.519709186788717],
              [-28.715237265474936, 38.519708482934256]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 452.39407018399999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15320,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_165",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.71546815825748, 38.519903022934656],
              [-28.715469757103286, 38.520106886071083],
              [-28.715277587565307, 38.520107351742105],
              [-28.715240393111632, 38.520107441934513],
              [-28.715238796043412, 38.519903722980644],
              [-28.71546815825748, 38.519903022934656]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 433.53958803299997,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15321,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_166",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715466626166599, 38.519707667804227],
              [-28.71546815825748, 38.519903022934656],
              [-28.715238796043412, 38.519903722980644],
              [-28.715237265474936, 38.519708482934256],
              [-28.715466626166599, 38.519707667804227]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 452.71446997700002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15322,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_167",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.71569752045022, 38.51990232243925],
              [-28.715699121076057, 38.520106329954793],
              [-28.715511042714734, 38.520106785985384],
              [-28.715469757103286, 38.520106886071083],
              [-28.71546815825748, 38.519903022934656],
              [-28.71569752045022, 38.51990232243925]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 433.79489168700002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15323,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_168",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715695986838011, 38.519706852454469],
              [-28.71569752045022, 38.51990232243925],
              [-28.71546815825748, 38.519903022934656],
              [-28.715466626166599, 38.519707667804227],
              [-28.715507910779596, 38.519707521034192],
              [-28.715695986838011, 38.519706852454469]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 453.03505993300001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15324,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_169",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.71592688262162, 38.519901621494483],
              [-28.715928485028567, 38.520105773410663],
              [-28.715699121076057, 38.520106329954793],
              [-28.71569752045022, 38.51990232243925],
              [-28.71592688262162, 38.519901621494483]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 434.04979267300001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15325,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_170",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715925347487726, 38.519706036705834],
              [-28.71592688262162, 38.519901621494483],
              [-28.71569752045022, 38.51990232243925],
              [-28.715695986838011, 38.519706852454469],
              [-28.715925347487726, 38.519706036705834]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 453.35437931199999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15326,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_171",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.71615624477165, 38.519900920100326],
              [-28.716157848952069, 38.520105215326318],
              [-28.715981158451022, 38.520105645139147],
              [-28.715980091592382, 38.520105648127085],
              [-28.715928485028567, 38.520105773410663],
              [-28.71592688262162, 38.519901621494483],
              [-28.71615624477165, 38.519900920100326]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 434.37945534400001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15327,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_172",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716154706145584, 38.519704969645154],
              [-28.71615624477165, 38.519900920100326],
              [-28.71592688262162, 38.519901621494483],
              [-28.715925347487726, 38.519706036705834],
              [-28.715976953576099, 38.519705853100653],
              [-28.716093267126027, 38.519705438982584],
              [-28.716154706145584, 38.519704969645154]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 453.67282819100001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15328,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_173",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716385606900335, 38.519900218256787],
              [-28.716387212856635, 38.520104656983648],
              [-28.716157848952069, 38.520105215326318],
              [-28.71615624477165, 38.519900920100326],
              [-28.716385606900335, 38.519900218256787]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 436.15502280300001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15329,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_174",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.71638405940033, 38.519703217312987],
              [-28.716385606900335, 38.519900218256787],
              [-28.71615624477165, 38.519900920100326],
              [-28.716154706145584, 38.519704969645154],
              [-28.71638405940033, 38.519703217312987]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 453.99159567300001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15330,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_175",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 76.52 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716614969007651, 38.5198995159639],
              [-28.716616576742389, 38.52010409840203],
              [-28.716449143131701, 38.520104506149139],
              [-28.716387212856635, 38.520104656983648],
              [-28.716385606900335, 38.519900218256787],
              [-28.716614969007651, 38.5198995159639]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 438.48696084800002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15331,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_176",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716613412627765, 38.519701464621818],
              [-28.716614969007651, 38.5198995159639],
              [-28.716385606900335, 38.519900218256787],
              [-28.71638405940033, 38.519703217312987],
              [-28.716445985194511, 38.519702744103085],
              [-28.716613412627765, 38.519701464621818]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 454.19562666500002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15332,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_177",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 81.5245 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.71684433109359, 38.519898813221616],
              [-28.716845938205282, 38.520103233792057],
              [-28.716784604296752, 38.520103624147637],
              [-28.716767967881182, 38.520103729516613],
              [-28.716616576742389, 38.52010409840203],
              [-28.716614969007651, 38.5198995159639],
              [-28.71684433109359, 38.519898813221616]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 440.81870779600001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15333,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_178",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 98.3 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716842765827423, 38.51969971151474],
              [-28.71684433109359, 38.519898813221616],
              [-28.716614969007651, 38.5198995159639],
              [-28.716613412627765, 38.519701464621818],
              [-28.716842765827423, 38.51969971151474]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 452.95041778000001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15334,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_179",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.717073693158145, 38.519898110029928],
              [-28.717075294951176, 38.520101771840551],
              [-28.716918184105445, 38.520102773946569],
              [-28.716845938205282, 38.520103233792057],
              [-28.71684433109359, 38.519898813221616],
              [-28.717073693158145, 38.519898110029928]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 443.15088109099997,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 615,
        IDUNIDADEA: 15335,
        UNIDADEAMO: "AHR_UA_RWY_1028_3_180",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.717072118994519, 38.519697957384601],
              [-28.717073693158145, 38.519898110029928],
              [-28.71684433109359, 38.519898813221616],
              [-28.716842765827423, 38.51969971151474],
              [-28.716915012438658, 38.519699159190239],
              [-28.717072118994519, 38.519697957384601]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 443.630967505,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15336,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_181",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.717817512407734, 38.519692254642223],
              [-28.717818305597145, 38.519792962650826],
              [-28.717359590105559, 38.519794900623523],
              [-28.717358809696108, 38.51969576376586],
              [-28.717817512407734, 38.519692254642223]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 432.13020449499999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15337,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_182",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.717819940297002, 38.520000179342247],
              [-28.717820703097846, 38.520097016899825],
              [-28.717361990250673, 38.520099942657602],
              [-28.71736122069068, 38.520002117392941],
              [-28.717819940297002, 38.520000179342247]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 450.61430046499999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15338,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_183",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.718276221698087, 38.51968874210722],
              [-28.718277027966327, 38.51979102172271],
              [-28.717818305597145, 38.519792962650826],
              [-28.717817512407734, 38.519692254642223],
              [-28.718276221698087, 38.51968874210722]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 427.74761331600001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15339,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_184",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.718278661475598, 38.519998238453518],
              [-28.718279417084013, 38.520094088831236],
              [-28.717820703097846, 38.520097016899825],
              [-28.717819940297002, 38.520000179342247],
              [-28.718278661475598, 38.519998238453518]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 457.58876777400002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15340,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_185",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.718734927651578, 38.519685227800117],
              [-28.718735746968012, 38.519789079011076],
              [-28.718277027966327, 38.51979102172271],
              [-28.718276221698087, 38.51968874210722],
              [-28.718734927651578, 38.519685227800117]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 423.36411685600001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15341,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_186",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.718737381793172, 38.519996295770717],
              [-28.718738130221766, 38.520091158970395],
              [-28.718279417084013, 38.520094088831236],
              [-28.718278661475598, 38.519998238453518],
              [-28.718737381793172, 38.519996295770717]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 419.47950730299999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15342,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_187",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2907
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.725157801373999, 38.519761693264826],
              [-28.725156990883765, 38.519660102900787],
              [-28.725437652494545, 38.519656800992458],
              [-28.725511603013448, 38.519668543045896],
              [-28.725568032894646, 38.51969213837107],
              [-28.725618082261004, 38.51972886543102],
              [-28.725643147086679, 38.519759608274619],
              [-28.725157801373999, 38.519761693264826]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 447.64509333400002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15343,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_188",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.724698230682534, 38.51976366476579],
              [-28.724697633128244, 38.519663110090704],
              [-28.724998803180512, 38.51966196144982],
              [-28.725156990883765, 38.519660102900787],
              [-28.725157801373999, 38.519761693264826],
              [-28.724698230682534, 38.51976366476579]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 446.18945240400001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15344,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_189",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.724240367036504, 38.519765627149354],
              [-28.724239564166066, 38.51966485564715],
              [-28.724697633128244, 38.519663110090704],
              [-28.724698230682534, 38.51976366476579],
              [-28.724240367036504, 38.519765627149354]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 429.83995306200001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15345,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_190",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.724242789506309, 38.520069675327889],
              [-28.724242018004695, 38.519972843826196],
              [-28.724699462117094, 38.51997088324886],
              [-28.724700040833334, 38.520068264459439],
              [-28.724242789506309, 38.520069675327889]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 447.88921163399999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15346,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_191",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.723781649145547, 38.519767591398171],
              [-28.723780845179352, 38.519666601884182],
              [-28.724239564166066, 38.51966485564715],
              [-28.724240367036504, 38.519765627149354],
              [-28.723781649145547, 38.519767591398171]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 427.17002443799998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15347,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_192",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.723784058866421, 38.520070280676478],
              [-28.723783298798022, 38.519974808073336],
              [-28.724242018004695, 38.519972843826196],
              [-28.724242789506309, 38.520069675327889],
              [-28.72417269155417, 38.520069891460224],
              [-28.723784058866421, 38.520070280676478]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.85706005499998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15348,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_193",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.723322931161725, 38.519769553849663],
              [-28.72332212610257, 38.519668346323776],
              [-28.723780845179352, 38.519666601884182],
              [-28.723781649145547, 38.519767591398171],
              [-28.723322931161725, 38.519769553849663]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 420.53888563800001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15349,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_194",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.723325326991223, 38.520070738436644],
              [-28.72332457949852, 38.519976770523158],
              [-28.723783298798022, 38.519974808073336],
              [-28.723784058866421, 38.520070280676478],
              [-28.723325326991223, 38.520070738436644]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 449.82490847600002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15350,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_195",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.722864213085114, 38.519771514503844],
              [-28.722863406935748, 38.519670088966073],
              [-28.72332212610257, 38.519668346323776],
              [-28.723322931161725, 38.519769553849663],
              [-28.722864213085114, 38.519771514503844]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 413.85829111700002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15351,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_196",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.722866595042319, 38.520071194398959],
              [-28.722865860106207, 38.519978731175627],
              [-28.72332457949852, 38.519976770523158],
              [-28.723325326991223, 38.520070738436644],
              [-28.722866595042319, 38.520071194398959]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 451.17954620299997,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15352,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_197",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.722405494915719, 38.519773473360694],
              [-28.722404686140333, 38.519671636077028],
              [-28.72281724358016, 38.519670264237867],
              [-28.722863406935748, 38.519670088966073],
              [-28.722864213085114, 38.519771514503844],
              [-28.722405494915719, 38.519773473360694]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 407.17769659700002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15353,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_198",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.722407863019757, 38.520071648563352],
              [-28.722407140621129, 38.519980690030792],
              [-28.722865860106207, 38.519978731175627],
              [-28.722866595042319, 38.520071194398959],
              [-28.722407863019757, 38.520071648563352]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 453.09893686700002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15354,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_199",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.721946776653603, 38.51977543042026],
              [-28.721945965085403, 38.519673159712511],
              [-28.722404686140333, 38.519671636077028],
              [-28.722405494915719, 38.519773473360694],
              [-28.721946776653603, 38.51977543042026]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 400.49710207599998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15355,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_200",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.72194913092358, 38.520072100929909],
              [-28.721948421043336, 38.519982647088597],
              [-28.722407140621129, 38.519980690030792],
              [-28.722407863019757, 38.520071648563352],
              [-28.72194913092358, 38.520072100929909]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 455.02317041399999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15356,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_201",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.721488058298842, 38.519777385682509],
              [-28.721487243943304, 38.519674681550526],
              [-28.721945965085403, 38.519673159712511],
              [-28.721946776653603, 38.51977543042026],
              [-28.721488058298842, 38.519777385682509]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 394.69060752600001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15357,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_202",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 90.25 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.721490406608122, 38.520073542029337],
              [-28.72148970137285, 38.519984602349112],
              [-28.721948421043336, 38.519982647088597],
              [-28.72194913092358, 38.520072100929909],
              [-28.721672754486573, 38.520072372603295],
              [-28.721490406608122, 38.520073542029337]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 456.94740395999997,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15358,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_203",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.721029339851409, 38.519779339147426],
              [-28.72102852271405, 38.519676201591039],
              [-28.721487243943304, 38.519674681550526],
              [-28.721488058298842, 38.519777385682509],
              [-28.721029339851409, 38.519779339147426]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 397.06513860699999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15359,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_204",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.721031694100965, 38.520076482569692],
              [-28.721030981609722, 38.519986555812288],
              [-28.72148970137285, 38.519984602349112],
              [-28.721490406608122, 38.520073542029337],
              [-28.721031694100965, 38.520076482569692]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 458.87163750799999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15360,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_205",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.720570621311385, 38.519781290815011],
              [-28.720569801397701, 38.519677719834185],
              [-28.72102852271405, 38.519676201591039],
              [-28.721029339851409, 38.519779339147426],
              [-28.720570621311385, 38.519781290815011]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 401.44748780600003,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15361,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_206",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.720572981488665, 38.520079421313028],
              [-28.720572261754008, 38.519988507478132],
              [-28.721030981609722, 38.519986555812288],
              [-28.721031694100965, 38.520076482569692],
              [-28.720572981488665, 38.520079421313028]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 460.795871035,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15362,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_207",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.720111902678791, 38.519783240685307],
              [-28.720111079994297, 38.519679236279778],
              [-28.720569801397701, 38.519677719834185],
              [-28.720570621311385, 38.519781290815011],
              [-28.720111902678791, 38.519783240685307]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 405.82983700599999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15363,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_208",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.720114268771265, 38.520082358259408],
              [-28.720113541805731, 38.519990457346637],
              [-28.720572261754008, 38.519988507478132],
              [-28.720572981488665, 38.520079421313028],
              [-28.720114268771265, 38.520082358259408]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 462.720104564,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15364,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_209",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 40.6655 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.719653183953685, 38.519785188758263],
              [-28.719652358503897, 38.519680750927961],
              [-28.720111079994297, 38.519679236279778],
              [-28.720111902678791, 38.519783240685307],
              [-28.719653183953685, 38.519785188758263]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 410.21218620500002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15365,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_210",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.71965555594884, 38.520085293408805],
              [-28.719654821764927, 38.519992405417824],
              [-28.720113541805731, 38.519990457346637],
              [-28.720114268771265, 38.520082358259408],
              [-28.71965555594884, 38.520085293408805]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 464.22897469200001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15366,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_211",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.719193636926505, 38.519682263778662],
              [-28.719194465136098, 38.519787135033916],
              [-28.718735746761727, 38.519789079509991],
              [-28.718734927939462, 38.51968522865149],
              [-28.719067331438055, 38.51968268001491],
              [-28.719193636926505, 38.519682263778662]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 414.594535404,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15367,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_212",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.719196843021383, 38.520088226761217],
              [-28.719196101631638, 38.519994351691679],
              [-28.719654821764927, 38.519992405417824],
              [-28.71965555594884, 38.520085293408805],
              [-28.719196843021383, 38.520088226761217]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 459.95188695100001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15368,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_213",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2907
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.725193498640621, 38.520066739764268],
              [-28.725187955642763, 38.519968787360213],
              [-28.725674396519032, 38.519966698323834],
              [-28.72567433920219, 38.520065250624491],
              [-28.725193498640621, 38.520066739764268]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 464.15011087900001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 653,
        IDUNIDADEA: 15369,
        UNIDADEAMO: "AHR_UA_RWY_1028_9_214",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2901
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.724700040833334, 38.520068264459439],
              [-28.724699462117094, 38.51997088324886],
              [-28.725159455033651, 38.519968909944744],
              [-28.725187955642763, 38.519968787360213],
              [-28.725193498640621, 38.520066739764268],
              [-28.725160235623886, 38.520066842705177],
              [-28.724700040833334, 38.520068264459439]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 384.53447724099999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15370,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_215",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.713173803469786, 38.519802832545636],
              [-28.713173123357485, 38.519715795559449],
              [-28.713631740770424, 38.519714173945317],
              [-28.713632414528345, 38.519800398752558],
              [-28.713173803469786, 38.519802832545636]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 438.33011156200001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15371,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_216",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.713176223606414, 38.520112542361723],
              [-28.713175454111351, 38.520014069493051],
              [-28.713615320534402, 38.520012293844381],
              [-28.713634067246343, 38.520011903934076],
              [-28.713634844328997, 38.52011132195365],
              [-28.713291382465606, 38.520112148113206],
              [-28.713176223606414, 38.520112542361723]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 388.22845640100002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15372,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_217",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.712715087406757, 38.519805265098491],
              [-28.71271440150435, 38.519717415745333],
              [-28.713173123357485, 38.519715795559449],
              [-28.713173803469786, 38.519802832545636],
              [-28.712715087406757, 38.519805265098491]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 436.57484980100003,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15373,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_218",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.712717498837009, 38.520114111689956],
              [-28.712716732158718, 38.520015919496799],
              [-28.713175454111351, 38.520014069493051],
              [-28.713176223606414, 38.520112542361723],
              [-28.712717498837009, 38.520114111689956]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 391.835118497,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15374,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_219",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.712256371245378, 38.519807695854233],
              [-28.712255679563171, 38.519719034133765],
              [-28.71271440150435, 38.519717415745333],
              [-28.712715087406757, 38.519805265098491],
              [-28.712256371245378, 38.519807695854233]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 435.32863481800001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15375,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_220",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.712258773980221, 38.520115679220744],
              [-28.712258010115097, 38.520017767703145],
              [-28.712716732158718, 38.520015919496799],
              [-28.712717498837009, 38.520114111689956],
              [-28.712258773980221, 38.520115679220744]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 395.44178059299998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15376,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_221",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.711797654985673, 38.5198101248128],
              [-28.711796957533988, 38.519720650724771],
              [-28.712255679563171, 38.519719034133765],
              [-28.712256371245378, 38.519807695854233],
              [-28.711797654985673, 38.5198101248128]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 434.08241983599999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15377,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_222",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 52.954 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2905
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.711800049036057, 38.520117244954051],
              [-28.711799287980579, 38.520019614112165],
              [-28.712258010115097, 38.520017767703145],
              [-28.712258773980221, 38.520115679220744],
              [-28.711800049036057, 38.520117244954051]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 399.04844267,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15378,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_223",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.711338938627684, 38.519812551974205],
              [-28.711338235416832, 38.519722265518304],
              [-28.711796957533988, 38.519720650724771],
              [-28.711797654985673, 38.5198101248128],
              [-28.711338938627684, 38.519812551974205]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 432.83595813900001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15379,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_224",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.7113413240046, 38.520118808889855],
              [-28.711340565756196, 38.520021458854082],
              [-28.711731765763528, 38.520019885745029],
              [-28.711799287980579, 38.520019614112165],
              [-28.711800049036057, 38.520117244954051],
              [-28.7113413240046, 38.520118808889855]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 402.71329485299998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15380,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_225",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.71088022217149, 38.519814977338463],
              [-28.710879511632605, 38.519723675594989],
              [-28.710938770806703, 38.519723670249221],
              [-28.711338235416832, 38.519722265518304],
              [-28.711338938627684, 38.519812551974205],
              [-28.71088022217149, 38.519814977338463]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 431.58907207499999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15381,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_226",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.71088259888586, 38.520120371028213],
              [-28.710881843441136, 38.520023301821162],
              [-28.711340565756196, 38.520021458854082],
              [-28.7113413240046, 38.520118808889855],
              [-28.71088259888586, 38.520120371028213]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 410.649754132,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15382,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_227",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 86.84 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.710421505617091, 38.519817400905559],
              [-28.710420777126263, 38.519723715962023],
              [-28.710879511632605, 38.519723675594989],
              [-28.71088022217149, 38.519814977338463],
              [-28.710421505617091, 38.519817400905559]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 430.707640438,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15383,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_228",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.710423876663526, 38.520122315058607],
              [-28.710423121035284, 38.520025142990825],
              [-28.710881843441136, 38.520023301821162],
              [-28.71088259888586, 38.520120371028213],
              [-28.710620700183938, 38.520121262089496],
              [-28.710423876663526, 38.520122315058607]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 421.21795756199998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15384,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_229",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.709962788517906, 38.519819765189453],
              [-28.709962042552036, 38.519723754531114],
              [-28.710420777126263, 38.519723715962023],
              [-28.710421505617091, 38.519817400905559],
              [-28.710008076617829, 38.519819583661771],
              [-28.709962788517906, 38.519819765189453]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 432.78387013100001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15385,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_230",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.709965158315992, 38.520124767831696],
              [-28.709964398538684, 38.520026982363177],
              [-28.710423121035284, 38.520025142990825],
              [-28.710423876663526, 38.520122315058607],
              [-28.709965158315992, 38.520124767831696]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 430.26355097200002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15386,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_231",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.709504067247238, 38.519821602888328],
              [-28.709503307909966, 38.519723791302212],
              [-28.709962042552036, 38.519723754531114],
              [-28.709962788517906, 38.519819765189453],
              [-28.709504067247238, 38.519821602888328]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 435.501125785,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15387,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_232",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.709506439869951, 38.520127218807616],
              [-28.709505676064147, 38.520028834466892],
              [-28.709591435005883, 38.520028476536631],
              [-28.709964398538684, 38.520026982363177],
              [-28.709965158315992, 38.520124767831696],
              [-28.709506439869951, 38.520127218807616]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 438.25918832500003,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15388,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_233",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 78.31 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.709045345885897, 38.5198234387898],
              [-28.709044573200075, 38.519723826275317],
              [-28.709503307909966, 38.519723791302212],
              [-28.709504067247238, 38.519821602888328],
              [-28.709045345885897, 38.5198234387898]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 437.99341983300002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15389,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_234",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.709047721325426, 38.52012966798636],
              [-28.709046953988885, 38.520030747958472],
              [-28.709505676064147, 38.520028834466892],
              [-28.709506439869951, 38.520127218807616],
              [-28.709047721325426, 38.52012966798636]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 443.93708718099998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15390,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_235",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.70858662443392, 38.519825272893939],
              [-28.708585849825234, 38.519725330681382],
              [-28.708911386028866, 38.519723836092446],
              [-28.709044573200075, 38.519723826275317],
              [-28.709045345885897, 38.5198234387898],
              [-28.70858662443392, 38.519825272893939]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 439.16313699400001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15391,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_236",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.708588998268858, 38.5201315459258],
              [-28.708588231822041, 38.520032659652728],
              [-28.709046953988885, 38.520030747958472],
              [-28.709047721325426, 38.52012966798636],
              [-28.70902755900098, 38.520129775595315],
              [-28.708588998268858, 38.5201315459258]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 443.11640304100001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15392,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_237",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.708127902891359, 38.519827105200683],
              [-28.708127131025272, 38.519727435196273],
              [-28.708585849825234, 38.519725330681382],
              [-28.70858662443392, 38.519825272893939],
              [-28.708127902891359, 38.519827105200683]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 438.899517946,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15393,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_238",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.708130274918375, 38.520133395888102],
              [-28.708129509563555, 38.52003456954963],
              [-28.708588231822041, 38.520032659652728],
              [-28.708588998268858, 38.5201315459258],
              [-28.708130274918375, 38.520133395888102]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 441.96265939699998,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15394,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_239",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.70766918241539, 38.519829085253569],
              [-28.70766841213128, 38.519729537913832],
              [-28.708127131025272, 38.519727435196273],
              [-28.708127902891359, 38.519827105200683],
              [-28.707745010888249, 38.519828633239143],
              [-28.70766918241539, 38.519829085253569]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 438.63334450299999,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15395,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_240",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 89.81 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.707671551477112, 38.520135244053051],
              [-28.707670787213562, 38.520036477649136],
              [-28.708129509563555, 38.52003456954963],
              [-28.708130274918375, 38.520133395888102],
              [-28.707671551477112, 38.520135244053051]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 443.37047266100001,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15396,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_241",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.707210468460914, 38.519831818605418],
              [-28.707209694088831, 38.519731638548485],
              [-28.70720980966319, 38.519731638300748],
              [-28.70766841213128, 38.519729537913832],
              [-28.70766918241539, 38.519829085253569],
              [-28.707210468460914, 38.519831818605418]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 438.48377036,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15397,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_242",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.707212830120259, 38.520137339191024],
              [-28.707212065193534, 38.520038383949576],
              [-28.707670787213562, 38.520036477649136],
              [-28.707671551477112, 38.520135244053051],
              [-28.707309949088017, 38.520136699656902],
              [-28.707212830120259, 38.520137339191024]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 448.65946781600002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15398,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_243",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.70675175362776, 38.519834550164866],
              [-28.706750966381751, 38.519732620891844],
              [-28.707209694088831, 38.519731638548485],
              [-28.707210468460914, 38.519831818605418],
              [-28.70675175362776, 38.519834550164866]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 470.24445610700002,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15399,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_244",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2908
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.706750966381751, 38.519732620891844],
              [-28.70675175362776, 38.519834550164866],
              [-28.706212922040301, 38.519837756508267],
              [-28.706275258331541, 38.519778164595415],
              [-28.7063417145663, 38.519746897050261],
              [-28.706415761206202, 38.519732326268013],
              [-28.706738091561228, 38.519732648436722],
              [-28.706750966381751, 38.519732620891844]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 439.95525662799997,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15400,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_245",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2906
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.706754115571957, 38.520140358764202],
              [-28.706753350125339, 38.520041254872858],
              [-28.707150754111588, 38.520038638602422],
              [-28.707212065193534, 38.520038383949576],
              [-28.707212830120259, 38.520137339191024],
              [-28.706754115571957, 38.520140358764202]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 542.94548793599995,
        IDZONA: 503,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 654,
        IDUNIDADEA: 15401,
        UNIDADEAMO: "AHR_UA_RWY_1028_10_246",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2908
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.706753350125339, 38.520041254872858],
              [-28.706754115571957, 38.520140358764202],
              [-28.7061873758385, 38.520144086950616],
              [-28.706188005891971, 38.520044974436622],
              [-28.706753350125339, 38.520041254872858]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 415.616879896,
        IDZONA: 506,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 624,
        IDUNIDADEA: 15442,
        UNIDADEAMO: "AHR_UA_TWY_A_2_6",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2902
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.714622485133177, 38.520177588539312],
              [-28.714611128547087, 38.520167638787385],
              [-28.714582210350567, 38.520152042503845],
              [-28.714546521982648, 38.520137611265149],
              [-28.714505445905743, 38.520125398416774],
              [-28.714478115496124, 38.520119388987283],
              [-28.714441621166927, 38.520109375012268],
              [-28.71527758756995, 38.520107352859185],
              [-28.715224537364765, 38.52012234046007],
              [-28.715177351375011, 38.520139052166954],
              [-28.715142372546389, 38.520154516363014],
              [-28.715106184834635, 38.520175885509055],
              [-28.714622485133177, 38.520177588539312]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 382.53947613499997,
        IDZONA: 506,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 625,
        IDUNIDADEA: 15443,
        UNIDADEAMO: "AHR_UA_TWY_A_1_1",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2902
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.714481400406395, 38.520188381129088],
              [-28.714425601811822, 38.520178281568299],
              [-28.714622485335543, 38.520177588963513],
              [-28.714649887150532, 38.520196829860865],
              [-28.714681473540949, 38.520226690580017],
              [-28.714693906693906, 38.520245347666865],
              [-28.714702285607295, 38.520257921386467],
              [-28.714723127075647, 38.52029036636246],
              [-28.714734736959709, 38.52032415765057],
              [-28.714735513887387, 38.52035689486371],
              [-28.714739264723445, 38.520580017431385],
              [-28.714742428373217, 38.520580033517057],
              [-28.714640516805726, 38.520582676364441],
              [-28.714648032287691, 38.520574075815865],
              [-28.714649057733176, 38.520552228206945],
              [-28.714652641619779, 38.520443012697108],
              [-28.71464550811169, 38.520337592952508],
              [-28.714630754527303, 38.520301421344101],
              [-28.714617889568963, 38.520279777178374],
              [-28.71461139956789, 38.520266530080633],
              [-28.714589274383187, 38.520245021538805],
              [-28.714562605410563, 38.520227218299695],
              [-28.714511041004062, 38.520201289187213],
              [-28.714481400406395, 38.520188381129088]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 401.910723281,
        IDZONA: 506,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 625,
        IDUNIDADEA: 15444,
        UNIDADEAMO: "AHR_UA_TWY_A_1_2",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 35.32 }],
        insp: "Sim",
        BERMA: 0,
        IDSG: 2902
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715083632723193, 38.520192902387038],
              [-28.715106184332413, 38.520175885886097],
              [-28.715347229872478, 38.52017503663761],
              [-28.715244057809418, 38.520186912815284],
              [-28.715202789657607, 38.520204498365224],
              [-28.715160208372563, 38.520231806531584],
              [-28.715127002464023, 38.520263829006375],
              [-28.71509722860624, 38.52031035622759],
              [-28.715087547456957, 38.520357802032095],
              [-28.715085870990308, 38.520417259796041],
              [-28.71508536369868, 38.520460932440287],
              [-28.715103607287315, 38.520514033733562],
              [-28.715133852596821, 38.5205524041193],
              [-28.715160809656144, 38.520582332235271],
              [-28.71504654040594, 38.520581580531882],
              [-28.715019295840541, 38.520539526888584],
              [-28.715006028922271, 38.520500908914983],
              [-28.714998933241109, 38.520462199983612],
              [-28.714996036728063, 38.520405235017989],
              [-28.714997973245371, 38.520356689584986],
              [-28.715001683135206, 38.520317820705323],
              [-28.715017710923178, 38.520277560274231],
              [-28.715034736425309, 38.520250361296242],
              [-28.715042998246609, 38.520237163055214],
              [-28.715064029026994, 38.520212596043216],
              [-28.715083632723193, 38.520192902387038]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 389.09684010900003,
        IDZONA: 506,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 624,
        IDUNIDADEA: 15445,
        UNIDADEAMO: "AHR_UA_TWY_A_2_3",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2902
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.714642624157893, 38.520191729861025],
              [-28.714622485133177, 38.520177588539312],
              [-28.715106184834635, 38.520175885509055],
              [-28.715083633216015, 38.520192901950132],
              [-28.715082459117415, 38.520194081473029],
              [-28.715064029832924, 38.520212595893895],
              [-28.715042998189006, 38.520237162471673],
              [-28.715034736065441, 38.520250361157203],
              [-28.715025377347658, 38.520265311638092],
              [-28.715018627782371, 38.52027609401339],
              [-28.715017710507492, 38.520277559352202],
              [-28.715015271256142, 38.520283686828293],
              [-28.714719693471238, 38.520285019998731],
              [-28.714718449885012, 38.520283084064921],
              [-28.714711570734398, 38.52027237505019],
              [-28.714702286132177, 38.520257921378807],
              [-28.714693907069389, 38.520245347512436],
              [-28.714681474101411, 38.520226690228057],
              [-28.714649887258652, 38.520196829807681],
              [-28.714642624157893, 38.520191729861025]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 394.93080715899998,
        IDZONA: 506,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 624,
        IDUNIDADEA: 15446,
        UNIDADEAMO: "AHR_UA_TWY_A_2_4",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2902
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.714736801657796, 38.520433526418941],
              [-28.714997342177274, 38.520430879807734],
              [-28.714997646081557, 38.520436857605787],
              [-28.714998311487335, 38.520449946134143],
              [-28.714998934390287, 38.520462198626142],
              [-28.715003012723134, 38.52048445110475],
              [-28.715006028905147, 38.520500908193377],
              [-28.715019296900913, 38.520539527319947],
              [-28.715046541717339, 38.520581580478321],
              [-28.714739264584065, 38.520580017089685],
              [-28.714737704031744, 38.520487198235855],
              [-28.714737203652881, 38.520457436491085],
              [-28.714736880132087, 38.520438193956444],
              [-28.714736801657796, 38.520433526418941]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 381.4821581,
        IDZONA: 506,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 624,
        IDUNIDADEA: 15447,
        UNIDADEAMO: "AHR_UA_TWY_A_2_5",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 27.138 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2902
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.714723126956358, 38.520290365023577],
              [-28.714719693471238, 38.520285019998731],
              [-28.715015271256142, 38.520283686828293],
              [-28.715001683132765, 38.52031782060223],
              [-28.714997973918312, 38.520356688440771],
              [-28.714997320502931, 38.52037307658334],
              [-28.7149960383592, 38.52040523368786],
              [-28.714996890153447, 38.520421988495379],
              [-28.714997342177274, 38.520430879807734],
              [-28.714736801657796, 38.520433526418941],
              [-28.714736691980821, 38.520427002989926],
              [-28.714735789135162, 38.520373302927197],
              [-28.714735513261989, 38.520356894322887],
              [-28.714734736326957, 38.520324156800442],
              [-28.714723126956358, 38.520290365023577]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 550.46204235300002,
        IDZONA: 505,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 622,
        IDUNIDADEA: 15404,
        UNIDADEAMO: "AHR_UA_TWY_B_1_5",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 17.38 }],
        insp: "Sim",
        BERMA: 1,
        IDSG: 2903
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716169667609076, 38.520200633972806],
              [-28.716583214249308, 38.520198632407023],
              [-28.716555296901635, 38.520231775399324],
              [-28.716541550450096, 38.520254425717418],
              [-28.716521659045206, 38.52029615979805],
              [-28.716508485957792, 38.520342975258181],
              [-28.716509347920208, 38.520379257976956],
              [-28.716509464824011, 38.520384178821963],
              [-28.716252194864847, 38.520388495772472],
              [-28.716251864894517, 38.520384864601297],
              [-28.716248012957401, 38.520342477756429],
              [-28.716242963300964, 38.520314923138471],
              [-28.716234782128847, 38.520294322965434],
              [-28.716214023334775, 38.520253184869162],
              [-28.716191334272001, 38.520223299444773],
              [-28.716169667609076, 38.520200633972806]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 381.443179144,
        IDZONA: 505,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 623,
        IDUNIDADEA: 15405,
        UNIDADEAMO: "AHR_UA_TWY_B_2_1",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 0,
        IDSG: 2903
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716588430315646, 38.520192438721935],
              [-28.716620673149706, 38.520169853503951],
              [-28.716797136090047, 38.520169045892629],
              [-28.71670164565754, 38.520203484649436],
              [-28.716657458013398, 38.520234381454834],
              [-28.716640909158645, 38.520258151975902],
              [-28.716607731605801, 38.520302333688363],
              [-28.716599895866036, 38.520332699076938],
              [-28.716597129795687, 38.520576422154853],
              [-28.716510747702507, 38.520576917117907],
              [-28.716511693818809, 38.520524239474767],
              [-28.716510801912055, 38.520440504217433],
              [-28.716508484929481, 38.52034297525605],
              [-28.716521658016902, 38.52029615979594],
              [-28.716541549071863, 38.520254425720417],
              [-28.716555295899468, 38.520231776496814],
              [-28.716588430315646, 38.520192438721935]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 384.91165235699998,
        IDZONA: 505,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 623,
        IDUNIDADEA: 15406,
        UNIDADEAMO: "AHR_UA_TWY_B_2_2",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 16.27 }],
        insp: "Sim",
        BERMA: 0,
        IDSG: 2903
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.715994628901999, 38.520182145046824],
              [-28.715954570813366, 38.520172893832459],
              [-28.716086193740214, 38.520172296563288],
              [-28.716094129729612, 38.52017225943041],
              [-28.716137331554208, 38.520172063104624],
              [-28.716162030444877, 38.520192645819449],
              [-28.716191334293455, 38.520223299427293],
              [-28.716214023356237, 38.52025318485164],
              [-28.716234782169892, 38.520294323772639],
              [-28.716242963322429, 38.520314923120971],
              [-28.716248012998463, 38.520342478563627],
              [-28.716252478144117, 38.520391624808809],
              [-28.716250452606673, 38.520445184800415],
              [-28.716248911824728, 38.520472835485243],
              [-28.716243993462633, 38.520497081967797],
              [-28.716237771335827, 38.520512713955313],
              [-28.716198657982655, 38.520578039831925],
              [-28.716089937964025, 38.520579044002602],
              [-28.716111550409106, 38.520555875279548],
              [-28.71613844508077, 38.520521744639801],
              [-28.716150260549977, 38.520494365184781],
              [-28.716160563910631, 38.52046156620586],
              [-28.716163505197798, 38.520410377586266],
              [-28.716161928995927, 38.520344016919658],
              [-28.716152767793364, 38.520308240136487],
              [-28.716109675488191, 38.520243576510858],
              [-28.716067357699163, 38.520211549453478],
              [-28.71603791825228, 38.520196746572118],
              [-28.715994628901999, 38.520182145046824]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 503.936253873,
        IDZONA: 505,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 622,
        IDUNIDADEA: 15407,
        UNIDADEAMO: "AHR_UA_TWY_B_1_3",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2903
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716252194864847, 38.520388495772472],
              [-28.716509464824011, 38.520384178821963],
              [-28.716509632706835, 38.520391245530945],
              [-28.716510671471585, 38.520434970304947],
              [-28.716510802920762, 38.520440503394831],
              [-28.716511693777747, 38.520524238667562],
              [-28.716510747681053, 38.520576917135415],
              [-28.716198659010956, 38.520578039834],
              [-28.716237771314365, 38.520512713972828],
              [-28.716243993441157, 38.520497081985305],
              [-28.716248911783669, 38.520472834678031],
              [-28.716250452565617, 38.520445183993239],
              [-28.716250662192884, 38.52043964463077],
              [-28.71625238478471, 38.520394118686745],
              [-28.716252479152828, 38.520391623986185],
              [-28.716252194864847, 38.520388495772472]
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        works: [],
        defects: [],
        AREA: 515.77958573299998,
        IDZONA: 505,
        TIPOPAVIME: "Flexivel",
        IDSECCAO: 622,
        IDUNIDADEA: 15408,
        UNIDADEAMO: "AHR_UA_TWY_B_1_4",
        NUMEROLAJE: 0,
        DATA: "2013/01/01",
        pci: [{ forecast: false, date: "2013/01/01", value: 100.0 }],
        insp: null,
        BERMA: 1,
        IDSG: 2903
      },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-28.716029321735522, 38.520121396791126],
              [-28.715981158429567, 38.520105645156626],
              [-28.716767967859706, 38.520103729534128],
              [-28.71678460427529, 38.520103624165145],
              [-28.716755785342492, 38.520108694826774],
              [-28.716730701507156, 38.520115215505719],
              [-28.716687518625534, 38.520129526120222],
              [-28.716648788909012, 38.520147871111419],
              [-28.716627426904203, 38.520164595772805],
              [-28.716620674158417, 38.520169852681335],
              [-28.716588431343965, 38.520192438724003],
              [-28.716588287338276, 38.520192609719643],
              [-28.716583214259199, 38.520198632414065],
              [-28.716169667583923, 38.520200633989504],
              [-28.716165233189908, 38.520195995129576],
              [-28.716162031473175, 38.520192645821581],
              [-28.71613733151317, 38.520172062297434],
              [-28.716128695631074, 38.52016438495761],
              [-28.716115498244555, 38.520158427781674],
              [-28.71609171439173, 38.520146466902126],
              [-28.716063543008516, 38.520133204685536],
              [-28.716029321735522, 38.520121396791126]
            ]
          ]
        ]
      }
    }
  ]
};
