import Vue from "vue";
import Vuetify from "vuetify/lib";
import "vuetify/src/stylus/app.styl";

Vue.use(Vuetify, {
  iconfont: "md",
  theme: {
    primary: "#5D858B",
    secondary: "#C6D4D6",
    accent: "#0D47A1",
    error: "#f44336",
    warning: "#FFAB00",
    info: "#00838F",
    success: "#4caf50"
  }
});
